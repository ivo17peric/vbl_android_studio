package com.cloverstudio.layouthelper;


public enum Resolution {
    IPHONE_4(960, 640), IPHONE_5(1136, 640), XHDPI(1280, 720);

    private int valueX;
    private int valueY;

    private Resolution(int x, int y) {
        this.valueX = x;
        this.valueY = y;
    }

    public int getValueX() {
        return this.valueX;
    }
    public int getValueY() {
        return this.valueY;
    }
}
