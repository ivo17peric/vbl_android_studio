package com.wolfshund.vbl;

import java.util.List;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wolfshund.api.GetRound;
import com.wolfshund.api.model.RoundData;
import com.wolfshund.api.model.RoundInformation;
import com.wolfshund.dialog.LoadingDialog;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.listeners.ErrorApiListener;
import com.wolfshund.utils.AnimUtils;
import com.wolfshund.utils.Const;
import com.wolfshund.utils.Utils;

public class RoundActivity extends BaseActivity {
	
	private List<RoundInformation> mData;
	private int round = -1;
	
	public static void start(Context c){
		Intent intent = new Intent(c, RoundActivity.class);
		startActivityWithAnimation(c, intent);
	}
	
	public static void start(Context c, int round){
		Intent intent = new Intent(c, RoundActivity.class);
		intent.putExtra(Const.Extra.ROUND, round);
		startActivityWithAnimation(c, intent);
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_round, R.id.parentRound);
        
        round = getIntent().getIntExtra(Const.Extra.ROUND, -1);
        
        new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// wait till override animation end
				checkForTutorial(Const.Activites.ROUNDLIST, new BaseActivity.CheckTutorialListener() {
					
					@Override
					public void onTutorialCheckFinish() {
						getRound(-1);
					}
				});
			}
		}, 300);
    }
    
    private void setData(){
    	final LinearLayout layout = (LinearLayout) findViewById(R.id.llForRoundList);
    	final FrameLayout contanier = (FrameLayout) findViewById(R.id.flForFading);
    	
    	AnimUtils.fadeSingleAnim(contanier, 1, 0, 500, new AnimatorListenerAdapter() {
			
			@Override
			public void onAnimationEnd(Animator animation) {
				addDataToLayout(layout);
				AnimUtils.fadeSingleAnim(contanier, 0, 1, 500);
			}
		});
    }
    
    private void addDataToLayout(LinearLayout layout){
    	TextView season = (TextView) findViewById(R.id.tvSeason);
    	season.setText("Season: " + mData.get(0).getSeason());
    	
    	TextView round = (TextView) findViewById(R.id.tvRound);
    	round.setText("Round: " + mData.get(0).getRound());
    	
    	TextView date = (TextView) findViewById(R.id.tvDate);
    	date.setText(String.valueOf(mData.get(0).getDateMatch()));
    	
    	layout.removeAllViews();
    	
    	int i = 1;
		for(RoundInformation item : mData){
			View view = LayoutInflater.from(this).inflate(R.layout.item_round_detailed, layout, false);
			Utils.scaleToFit(this, view);
			
			Utils.populateLastFive(this, (LinearLayout) view.findViewById(R.id.llLastFiveHome), item.getHomeLastFiveForm(), k);
			Utils.populateLastFive(this, (LinearLayout) view.findViewById(R.id.llLastFiveAway), item.getAwayLastFiveForm(), k);
			
			TextView tvOffDefHome = (TextView) view.findViewById(R.id.tvHomeOffDef);
			tvOffDefHome.setText(item.getHomeOffence()+"/"+item.getHomeDefence());
			
			TextView tvHome = (TextView) view.findViewById(R.id.tvHomeTeam);
			tvHome.setText(item.getHome());
			
			TextView tvOffDefAway = (TextView) view.findViewById(R.id.tvAwayOffDef);
			tvOffDefAway.setText(item.getAwayOffence()+"/"+item.getAwayDefence());
			
			TextView tvAway = (TextView) view.findViewById(R.id.tvAwayTeam);
			tvAway.setText(item.getAway());
			
			if(item.getAwayGoals() != RoundPagerFragment.NO_RESULT){
				TextView tvHomeScore = (TextView) view.findViewById(R.id.tvHomeScore);
				tvHomeScore.setText(String.valueOf(item.getHomeGoals()));
				TextView tvAwayScore = (TextView) view.findViewById(R.id.tvAwayScore);
				tvAwayScore.setText(String.valueOf(item.getAwayGoals()));
			}
			
			if(i % 2 == 0){
				view.setBackgroundColor(getResources().getColor(R.color.even_row));
			}else{
				view.setBackgroundColor(getResources().getColor(R.color.odd_row));
			}
			
			i++;
			
			layout.addView(view);
		}
    }
    
    private void setArrowsAndButton(){
    	TextView next = (TextView) findViewById(R.id.tvNextRound);
    	TextView prev = (TextView) findViewById(R.id.tvPrevRound);
    	TextView placeBet = (TextView) findViewById(R.id.btnPlaceBet);
    	
    	if(round < 2){
    		next.setVisibility(View.VISIBLE);
    		prev.setVisibility(View.INVISIBLE);
    	}else if(round > 29){
    		next.setVisibility(View.INVISIBLE);
    		prev.setVisibility(View.VISIBLE);
    	}else{
    		next.setVisibility(View.VISIBLE);
    		prev.setVisibility(View.VISIBLE);
    	}
    	
    	next.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				round++;
				getRound(round - 1);
			}
		});
    	
    	prev.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				round--;
				getRound(round + 1);
			}
		});
    	
    	if(mData.get(0).getHomeGoals() != RoundPagerFragment.NO_RESULT){
    		placeBet.setVisibility(View.INVISIBLE);
    	}else{
    		placeBet.setVisibility(View.VISIBLE);
    	}
    	
    	placeBet.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				PlaceBetActivity.start(RoundActivity.this, round);
			}
		});
    }
    
    private void getRound(final int oldRound){
    	final LoadingDialog dialog = new LoadingDialog(this);
    	dialog.show();
    	
    	GetRound client = new GetRound(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				mData = ((RoundData)result).getRoundInformationList();
				setData();
				setArrowsAndButton();
				dialog.dismiss();
			}
			
			@Override
			public void onError(String errorMessage) {
				dialog.dismiss();
			}
		});
    	
    	client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				dialog.show();
			}
			
			@Override
			public void onCancel() {
				if(round == -1){
					finishWithanimation(RoundActivity.this);
				}else{
					round = oldRound;
				}
			}
		});
    	
    	client.runClient(round, 0);
    }
    
    @Override
    public void onBackPressed() {
    	finishWithanimation(this);
    }
    
}
