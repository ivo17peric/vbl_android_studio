package com.wolfshund.vbl;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wolfshund.api.model.MasterData;
import com.wolfshund.utils.Const;

public class UserDataFragment extends BaseFragment{
	
	private MasterData mData;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view = setView(R.layout.fragment_user_data, R.id.parentUserDataFragment, inflater, container);
		
		view.findViewById(R.id.viewClickableArea).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (getActivity() instanceof HomeActivity) {
					((HomeActivity) getActivity()).startNextActivity(Const.Home.USER_DATA);
				}
			}
		});
		
		return view;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mData = (MasterData) getArguments().getSerializable(Const.Extra.MASTER);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		TextView userName = (TextView) getView().findViewById(R.id.tvUserName);
		userName.setText(mData.getMasterInformation().getUser().getUsername());
		
		TextView myPoints = (TextView) getView().findViewById(R.id.tvMyPoints);
		myPoints.setText(String.valueOf(mData.getMasterInformation().getUser().getpPoints()));
		
		TextView allTimePoints = (TextView) getView().findViewById(R.id.tvAllTimePoints);
		allTimePoints.setText(String.valueOf(mData.getMasterInformation().getUser().getPoints()));
		
		TextView tenPoint = (TextView) getView().findViewById(R.id.tvTenGames);
		tenPoint.setText(String.valueOf(mData.getMasterInformation().getUser().getPointsTenGames()));
		
		TextView lastTenPoint = (TextView) getView().findViewById(R.id.tvLastTenGames);
		lastTenPoint.setText(String.valueOf(mData.getMasterInformation().getUser().getPointsTenGamesLast()));
	}
	
}
