package com.wolfshund.vbl;

import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wolfshund.api.model.MasterData;
import com.wolfshund.api.model.RoundInformation;
import com.wolfshund.utils.Const;
import com.wolfshund.utils.Utils;

public class RoundPagerFragment extends BaseFragment{
	
	public static final int NO_RESULT = 99;
	
	private int mPageNumber;
	private List<RoundInformation> mRoundList;
	
	public static RoundPagerFragment create(int pageNumber, MasterData data) {
		RoundPagerFragment fragment = new RoundPagerFragment();
		Bundle args = new Bundle();
		args.putInt(Const.Extra.PAGE, pageNumber);
		args.putSerializable(Const.Extra.MASTER, data);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPageNumber = getArguments().getInt(Const.Extra.PAGE);
		MasterData data = (MasterData) getArguments().getSerializable(Const.Extra.MASTER);
		if (mPageNumber == 0) mRoundList = data.getMasterInformation().getPrevRound();
		if (mPageNumber == 1) mRoundList = data.getMasterInformation().getCurrentRound();
		if (mPageNumber == 2) mRoundList = data.getMasterInformation().getNextRound();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view = setView(R.layout.fragment_round_pager, R.id.parentRoundPagerFragment, inflater, container);
		
		view.findViewById(R.id.viewClickableArea).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if( mRoundList == null || mRoundList.size() == 0 ) return;
				if(getActivity() instanceof HomeActivity){
					((HomeActivity)getActivity()).startNextActivity(Const.Home.ROUND);
				}
			}
		});	
		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		TextView tvRound = (TextView) getView().findViewById(R.id.tvRound);
		if(mRoundList == null || mRoundList.size() == 0){
			if (mPageNumber == 0) tvRound.setText("No previous round");
			if (mPageNumber == 1) tvRound.setText("Waiting for round");
			if (mPageNumber == 2) tvRound.setText("Waiting for round");
			return;
		}
		tvRound.setText("Round: " + mRoundList.get(0).getRound());
		
		TextView tvDate = (TextView) getView().findViewById(R.id.tvDate);
		tvDate.setText(mRoundList.get(0).getDateMatch());
		
		LinearLayout layout = (LinearLayout) getView().findViewById(R.id.llInScrollViewRound);
		
		for(RoundInformation item : mRoundList){
			View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_round, layout, false);
			Utils.scaleToFit(getActivity(), view);
			
			TextView tvHomeName = (TextView) view.findViewById(R.id.tvHomeTeam);
			tvHomeName.setText(item.getHome());
			TextView tvAwayName = (TextView) view.findViewById(R.id.tvAwayTeam);
			tvAwayName.setText(item.getAway());
			
			if(item.getAwayGoals() != NO_RESULT){
				TextView tvHomeScore = (TextView) view.findViewById(R.id.tvHomeScore);
				tvHomeScore.setText(String.valueOf(item.getHomeGoals()));
				TextView tvAwayScore = (TextView) view.findViewById(R.id.tvAwayScore);
				tvAwayScore.setText(String.valueOf(item.getAwayGoals()));
			}
			
			layout.addView(view);
		}
	}
	
	public int getPageNumber() {
		return mPageNumber;
	}
	
}
