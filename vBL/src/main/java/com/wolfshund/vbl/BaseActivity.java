package com.wolfshund.vbl;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.wolfshund.dialog.AppDialog;
import com.wolfshund.listeners.DialogListener;
import com.wolfshund.utils.Const;
import com.wolfshund.utils.Preferences;
import com.wolfshund.utils.Utils;

public class BaseActivity extends Activity {
	
	private boolean isActive=false;
	private Preferences pref;
	protected float k;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		isActive=true;

		Utils.initializeK(this);
		k=Utils.getK();
		
		pref = new Preferences(this);
		
	}
	
	public Preferences getPreferences() {
		return pref;
	}
	
	protected void setContentView(int layoutResID, int viewToScaleID) {
		super.setContentView(layoutResID);
		scaleToFit(viewToScaleID);
	}
	
	protected void scaleToFit(int viewToScaleID) {
		Utils.getLayoutHelper().scaleToFit(findViewById(viewToScaleID), this);
	}
	
	protected boolean isActive() {
		return isActive;
	}
	
	protected static void startActivityWithAnimation(Context c, Intent i){
		c.startActivity(i);
		((Activity)c).overridePendingTransition(R.anim.move_in, R.anim.move_out);
	}
	
	protected static void finishWithanimation(Activity a){
		a.finish();
		a.overridePendingTransition(R.anim.close_in, R.anim.close_out);
	}
	
	@Override
	protected void onPause() {
		isActive = false;
		super.onPause();
	}
	
	@Override
	protected void onResume() {
		isActive = true;
		super.onResume();
	}
	
	protected void checkForTutorial(String activity, CheckTutorialListener lis) {
		if(!getPreferences().getCustomBoolean(activity)){
			showTutorialDialog(activity, lis);
		}else{
			lis.onTutorialCheckFinish();
		}
	}
	
	private void showTutorialDialog(String activity, final CheckTutorialListener lis){
		String text = "";
		if(activity.equals(Const.Activites.HOME)){
			text = getString(R.string.home_tutorial);
		}else if(activity.equals(Const.Activites.USERDATA)){
			text = getString(R.string.user_data_tutorial);
		}else if(activity.equals(Const.Activites.BETSLIST)){
			text = getString(R.string.user_bets_tutrial);
		}else if(activity.equals(Const.Activites.ROUNDLIST)){
			text = getString(R.string.round_tutorial);
		}else if(activity.equals(Const.Activites.USERSLIST)){
			text = getString(R.string.user_list_tutorial);
		}else if(activity.equals(Const.Activites.PLACEBET)){
			text = getString(R.string.place_bet_tutorial);
		}else if(activity.equals(Const.Activites.TABLE)){
			text = getString(R.string.on_this_screen_user_can_see_league_table_);
		}
		
		getPreferences().setCustomBoolean(activity, true);
		
		AppDialog dialog = new AppDialog(this, text, getString(R.string.close), null);
		dialog.setListener(new DialogListener() {
			
			@Override
			public void onOkClicked(Dialog d) {
				lis.onTutorialCheckFinish();
				d.dismiss();
			}
			
			@Override
			public void onCancelClicked(Dialog d) {}
		});
		dialog.show();
	}
	
	public interface CheckTutorialListener{
		public void onTutorialCheckFinish();
	}

}
