package com.wolfshund.vbl;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wolfshund.api.model.MasterData;
import com.wolfshund.api.model.UserBetsInformation;
import com.wolfshund.utils.Const;
import com.wolfshund.utils.Utils;

public class BetsFragment extends BaseFragment {
	
	private static final int WIN = 1;
	private static final int LOSE = 2;
	private static final int ONE_PAIR = 3;
	
	private MasterData mData;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = setView(R.layout.fragment_bets, R.id.parentBetsFragment, inflater, container);

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (getActivity() instanceof HomeActivity) {
					((HomeActivity) getActivity()).startNextActivity(Const.Home.BETS);
				}
			}
		});

		return view;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mData = (MasterData) getArguments().getSerializable(Const.Extra.MASTER);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		LinearLayout layout = (LinearLayout) getView().findViewById(R.id.llBets);
		int i = 1;
		for(UserBetsInformation item : mData.getMasterInformation().getBets()){
			View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_bets, layout, false);
			Utils.scaleToFit(getActivity(), view);
			
			TextView tvBetPlace = (TextView) view.findViewById(R.id.tvBetValue);
			tvBetPlace.setText(String.valueOf(item.getBetPlace()));
			
			TextView tvCoeff = (TextView) view.findViewById(R.id.tvCoefValue);
			tvCoeff.setText(String.valueOf(item.getKoef()));
			
			TextView win = (TextView) view.findViewById(R.id.tvWinValue);
			double winDouble = item.getBetPlace() * item.getKoef(); 
			win.setText(String.format("%.2f", winDouble));
			
			TextView halfWin = (TextView) view.findViewById(R.id.tvHalfWinValue);
			if(Utils.getBetSize(item.getBet()) > 4){
				double halfWinDouble = winDouble / item.getMaxKoef(); 
				halfWin.setText(String.format("%.2f", halfWinDouble));
			}else{
				halfWin.setText("-");
			}
			
			switch (item.getResult()) {
			case WIN:
				win.setTextColor(getResources().getColor(R.color.win_green));
				halfWin.setTextColor(getResources().getColor(R.color.lose_red));
				break;
				
			case LOSE:
				halfWin.setTextColor(getResources().getColor(R.color.lose_red));
				win.setTextColor(getResources().getColor(R.color.lose_red));
				break;
			case ONE_PAIR:
				halfWin.setTextColor(getResources().getColor(R.color.win_green));
				win.setTextColor(getResources().getColor(R.color.lose_red));
				break;

			default:
				break;
			}
			
			if(i % 2 == 0){
				view.setBackgroundColor(getResources().getColor(R.color.even_row));
			}else{
				view.setBackgroundColor(getResources().getColor(R.color.odd_row));
			}
			
			i++;
			
			layout.addView(view);
		}
	}

}
