package com.wolfshund.vbl;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wolfshund.utils.AnimUtils;

public class SplashActivity extends BaseActivity {
	
	private Handler h = new Handler();
	private Runnable r;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash, R.id.parentSplash);
        
        ImageView logo = (ImageView) findViewById(R.id.ivLogo);
        AnimUtils.fadeSingleAnim(logo, 0, 1, 500);
        AnimUtils.scaleShape(logo, 3f, 1f, 500, null);
        AnimUtils.rotation(logo, 180, 0, 500, 0);
        
        AnimUtils.translateX(logo, 0, -300 * k, 500, 500, new AnimatorListenerAdapter() {
        	
        	@Override
        	public void onAnimationEnd(Animator animation) {
        		TextView tvzName = (TextView) findViewById(R.id.tvzName);
        		tvzName.setVisibility(View.VISIBLE);
        		AnimUtils.fadeSingleAnim(tvzName, 0, 1, 500, new AnimatorListenerAdapter() {
        			
        			@Override
        			public void onAnimationEnd(Animator animation) {
        				if(isActive())
        					h.postDelayed(r, 1000);
        			}
				});
        	}
		});
        
        r = new Runnable() {
			
			@Override
			public void run() {
				if(TextUtils.isEmpty(getPreferences().getUuid())){
					SignUpActivity.start(SplashActivity.this);
				}else{
					HomeActivity.start(SplashActivity.this);
				}
				finish();
			}
		};
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	if(findViewById(R.id.tvzName).getVisibility() == View.VISIBLE){
    		h.postDelayed(r, 2000);
    	}
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	h.removeCallbacks(r);
    }
    
}
