package com.wolfshund.vbl;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wolfshund.api.GetBetRound;
import com.wolfshund.api.PostBet;
import com.wolfshund.api.model.BetRoundData;
import com.wolfshund.api.model.BetRoundInformation;
import com.wolfshund.api.model.TagBetModel;
import com.wolfshund.dialog.AppDialog;
import com.wolfshund.dialog.InfoPlaceBetDialog;
import com.wolfshund.dialog.LoadingDialog;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.listeners.DialogListener;
import com.wolfshund.listeners.ErrorApiListener;
import com.wolfshund.utils.Const;
import com.wolfshund.utils.Utils;

public class PlaceBetActivity extends BaseActivity {
	
	private List<BetRoundInformation> mData;
	private int round = -1;
	private LinearLayout mLayout;
	private double mAllCoef = 1;
	private int mPlaceBet = 10;
	private double mMaxCoef = 1;
	private List<Double> mCoefList = new ArrayList<Double>(); 
	private List<BetModelLocal> mBetModelList = new ArrayList<BetModelLocal>(); 
	private long mMyPoints;
	
	private TextView mTvCoef;
	private TextView mTvHalfWin;
	private TextView mTvWin;
	private EditText mEtBet;
	private Button mSendBet;
	
	public static void start(Context c){
		Intent intent = new Intent(c, PlaceBetActivity.class);
		startActivityWithAnimation(c, intent);
	}
	
	public static void start(Context c, int round){
		Intent intent = new Intent(c, PlaceBetActivity.class);
		intent.putExtra(Const.Extra.ROUND, round);
		startActivityWithAnimation(c, intent);
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_bet, R.id.parentPlaceBet);
        
        round = getIntent().getIntExtra(Const.Extra.ROUND, -1);
        
        new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// wait till override animation end
				checkForTutorial(Const.Activites.PLACEBET, new BaseActivity.CheckTutorialListener() {
					
					@Override
					public void onTutorialCheckFinish() {
						getRoundForBet();
					}
				});
			}
		}, 300);
    }
    
    private void setData(){
    	mLayout = (LinearLayout) findViewById(R.id.llForRoundList);
    	TextView date = (TextView) findViewById(R.id.tvDate);
    	date.setText(String.valueOf(mData.get(0).getDateMatch()));
    	
    	mLayout.removeAllViews();
    	
    	int i = 1;
		for(final BetRoundInformation item : mData){
			View view = LayoutInflater.from(this).inflate(R.layout.item_place_bet, mLayout, false);
			Utils.scaleToFit(this, view);
			
			TextView tvHome = (TextView) view.findViewById(R.id.tvHomeTeam);
			tvHome.setText(item.getHomeName());
			
			TextView tvAway = (TextView) view.findViewById(R.id.tvAwayTeam);
			tvAway.setText(item.getAwayName());
			
			Button btnHomeWin = (Button) view.findViewById(R.id.btnHomeWin);
			Button btnDraw = (Button) view.findViewById(R.id.btnDraw);
			Button btnAwayWin = (Button) view.findViewById(R.id.btnAwayWin);
			Button btnHomeOrDraw = (Button) view.findViewById(R.id.btnHomeOrDraw);
			Button btnAwayOrDraw = (Button) view.findViewById(R.id.btnAwayOrDraw);
			Button btnHendHome = (Button) view.findViewById(R.id.btnHendHome);
			Button btnHendAway = (Button) view.findViewById(R.id.btnHendAway);
			
			btnHomeWin.setTag(new TagBetModel(item.getMatchId(), item.getBets().getHomeWin(), i, 3, -1, Const.BetType.HOME_WIN));
			btnDraw.setTag(new TagBetModel(item.getMatchId(), item.getBets().getDraw(), i, 4, -1, Const.BetType.DRAW));
			btnAwayWin.setTag(new TagBetModel(item.getMatchId(), item.getBets().getAwayWin(), i, 5, -1, Const.BetType.AWAY_WIN));
			btnHomeOrDraw.setTag(new TagBetModel(item.getMatchId(), item.getBets().getHomeDrawWin(), i, 6, -1, Const.BetType.HOME_WIN_OR_DRAW));
			btnAwayOrDraw.setTag(new TagBetModel(item.getMatchId(), item.getBets().getAwayDrawWin(), i, 7, -1, Const.BetType.AWAY_WIN_OR_DRAW));
			btnHendHome.setTag(new TagBetModel(item.getMatchId(), item.getBets().getHomeHendicap(), i, 8, -1, Const.BetType.HOME_HENDICEP));
			btnHendAway.setTag(new TagBetModel(item.getMatchId(), item.getBets().getAwayHendicap(), i, 9, -1, Const.BetType.AWAY_HENDICEP));
			
			btnHomeWin.setOnClickListener(onBetClick);
			btnDraw.setOnClickListener(onBetClick);;
			btnAwayWin.setOnClickListener(onBetClick);;
			btnHomeOrDraw.setOnClickListener(onBetClick);;
			btnAwayOrDraw.setOnClickListener(onBetClick);;
			btnHendHome.setOnClickListener(onBetClick);;
			btnHendAway.setOnClickListener(onBetClick);;
			
			btnHomeWin.setText(String.valueOf(item.getBets().getHomeWin()));
			btnDraw.setText(String.valueOf(item.getBets().getDraw()));
			btnAwayWin.setText(String.valueOf(item.getBets().getAwayWin()));
			btnHomeOrDraw.setText(String.valueOf(item.getBets().getHomeDrawWin()));
			btnAwayOrDraw.setText(String.valueOf(item.getBets().getAwayDrawWin()));
			btnHendHome.setText(String.valueOf(item.getBets().getHomeHendicap()));
			btnHendAway.setText(String.valueOf(item.getBets().getAwayHendicap()));
			
			Button info = (Button) view.findViewById(R.id.btnInfo);
			final int j = i-1;
			info.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					InfoPlaceBetDialog dialog = new InfoPlaceBetDialog(PlaceBetActivity.this);
					dialog.setData(item, j);
					dialog.show();
				}
			});
			
			if(i % 2 == 0){
				view.setBackgroundColor(getResources().getColor(R.color.even_row));
			}else{
				view.setBackgroundColor(getResources().getColor(R.color.odd_row));
			}
			
			i++;
			
			mLayout.addView(view);
		}
		
		setFooterDataAndView();
    }
    
    private void setFooterDataAndView(){
    	mTvCoef = (TextView) findViewById(R.id.coefValue);
    	mTvHalfWin = (TextView) findViewById(R.id.halfWinValue);
    	mTvWin = (TextView) findViewById(R.id.winValue);
    	mEtBet = (EditText) findViewById(R.id.etBetValue);
    	
    	mEtBet.setText(String.valueOf(mPlaceBet));
    	mTvHalfWin.setText("-");
    	mTvWin.setText(String.valueOf(mPlaceBet * mAllCoef));
    	mTvCoef.setText(String.valueOf(mAllCoef));
    	
    	mSendBet = (Button) findViewById(R.id.btnSendBet);
    	
    	mEtBet.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {
				try {
					mPlaceBet = Integer.parseInt(s.toString());
				} catch (Exception e) {
					mEtBet.setText(String.valueOf(mPlaceBet));
				}
				changeValueInFooter();
			}
		});
    	
    	mSendBet.setOnClickListener(onSendClick);
    	toEnableSendButton(false);
    }
    
    private void changeValueInFooter(){
    	mMaxCoef = 1;
    	mAllCoef = 1;
    	for(Double item : mCoefList){
    		if(item > mMaxCoef) mMaxCoef = item;
    		mAllCoef = mAllCoef * item;
    	}
    	
    	mTvWin.setText(String.format("%.2f", (mAllCoef * mPlaceBet)));
    	mTvCoef.setText(String.format("%.2f", mAllCoef));
    	if(mCoefList.size() > 4){
    		mTvHalfWin.setText(String.format("%.2f", (mAllCoef * mPlaceBet) / mMaxCoef));
    	}else{
    		mTvHalfWin.setText("-");
    	}
    	
    	if(mAllCoef * mPlaceBet > 50000){
    		mTvWin.setTextColor(getResources().getColor(R.color.lose_red));
    	}else{
    		mTvWin.setTextColor(Color.WHITE);
    	}
    	
    	if(mPlaceBet < 10 || mPlaceBet > 10000 || mPlaceBet > mMyPoints){
    		mEtBet.setTextColor(getResources().getColor(R.color.lose_red));
    	}else{
    		mEtBet.setTextColor(Color.BLACK);
    	}
    	
    	if(mAllCoef * mPlaceBet < 50000 && mPlaceBet >= 10 && mPlaceBet <= 10000 && mCoefList.size() > 0 && mPlaceBet <= mMyPoints){
    		toEnableSendButton(true);
    	}else{
    		toEnableSendButton(false);
    	}
    }
    
    private void removeFromList(double coef){
    	for(int i = 0; i < mCoefList.size(); i++){
    		if(coef == mCoefList.get(i)){
    			mCoefList.remove(i);
    			return;
    		}
    	}
    }
    
    private void addToList(double coef){
    	mCoefList.add(coef);
    }
    
    private void removeFromListBet(int matchId){
    	for(int i = 0; i < mBetModelList.size(); i++){
    		if(matchId == mBetModelList.get(i).getMatchId()){
    			mBetModelList.remove(i);
    			return;
    		}
    	}
    }
    
    private void addToListBet(BetModelLocal model){
    	mBetModelList.add(model);
    }
    
    private void toEnableSendButton(boolean toEnable){
    	mSendBet.setTag(toEnable);
    }
    
    private String createStringBet(){
    	StringBuilder betsBuilder = new StringBuilder();
    	for(BetModelLocal item : mBetModelList){
    		betsBuilder.append(item.getMatchId()).append("?").append(item.getBetType()).append(";");
    	}
    	String bets = betsBuilder.toString();
    	return bets.substring(0, bets.length()-1);
    }
    
    private View.OnClickListener onSendClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if((Boolean)mSendBet.getTag()){
				sendBet(createStringBet());
			}else{
				String message = "Unknown error!!!";
				if(mCoefList.size() == 0){
					message = "No bets selected!!!";
				}else if(mAllCoef * mPlaceBet >= 50000){
					message = "Win has to be below 50000!!!";
				}else if(mPlaceBet < 10){
					message = "Place bet has to be over 9!!!";
				}else if(mPlaceBet > 10000){
					message = "Place bet has to be below 10000!!!";
				}else if(mPlaceBet > mMyPoints){
					message = "Not enough points!!!";
				}
				AppDialog dialog = new AppDialog(PlaceBetActivity.this, message, "OK", "");
				dialog.setListener(new DialogListener() {
					
					@Override
					public void onOkClicked(Dialog dialog) {
						dialog.dismiss();
					}
					
					@Override
					public void onCancelClicked(Dialog dialog) {}
				});
				dialog.show();
			}
		}
	};
    
    private View.OnClickListener onBetClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			TagBetModel tag = (TagBetModel)v.getTag();
			ViewGroup group = (ViewGroup) mLayout.getChildAt(tag.getPos()-1);
			if(tag.getSelectedPosBetType() != -1 && tag.getSelectedPosBetType() == tag.getPosBetType()){
				v.setBackgroundColor(Color.TRANSPARENT);
				((TagBetModel)v.getTag()).setSelectedPosBetType(-1);
				removeFromList(tag.getCoef());
				removeFromListBet(tag.getMatchId());
				changeValueInFooter();
				return;
			}else{
				int check = checkForOtherInLinearLayout(tag.getPos()-1);
				if(check != -1){
					((Button)getChildAt(group).getChildAt(check)).setBackgroundColor(Color.TRANSPARENT);
					((TagBetModel)getChildAt(group).getChildAt(check).getTag()).setSelectedPosBetType(-1);
					removeFromList(((TagBetModel)getChildAt(group).getChildAt(check).getTag()).getCoef());
					removeFromListBet(tag.getMatchId());
				}
			}
			((TagBetModel)v.getTag()).setSelectedPosBetType(tag.getPosBetType());
			addToList(tag.getCoef());
			addToListBet(new BetModelLocal(tag.getMatchId(), tag.getBetType()));
			getChildAt(group).getChildAt(tag.getPosBetType()).setBackgroundResource(R.drawable.button);//((Button)group.getChildAt(tag.getPosBetType())).setBackgroundResource(R.drawable.button);
			changeValueInFooter();
		}
	}; 
	
	private int checkForOtherInLinearLayout(int pos){
		ViewGroup group = (ViewGroup) mLayout.getChildAt(pos);
		for(int i = 3; i < getChildAt(group).getChildCount(); i++){
			if(((TagBetModel)getChildAt(group).getChildAt(i).getTag()).getSelectedPosBetType() != -1){
				return i;
			}
		}
		return -1;
	}
    
    private void getRoundForBet(){
    	final LoadingDialog dialog = new LoadingDialog(this);
    	dialog.show();
    	
    	GetBetRound client = new GetBetRound(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				mData = ((BetRoundData)result).getDataM().getBetRoundInformationList();
				mMyPoints = ((BetRoundData)result).getDataM().getpPoints();
				setData();
				dialog.dismiss();
			}
			
			@Override
			public void onError(String errorMessage) {
				dialog.dismiss();
			}
		});
    	
    	client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				dialog.show();
			}
			
			@Override
			public void onCancel() {
				finishWithanimation(PlaceBetActivity.this);
			}
		});
    	
    	client.runClient(round, 0, getPreferences().getUuid());
    }
    
    private void sendBet(String bets){
    	final LoadingDialog dialog = new LoadingDialog(this);
    	dialog.show();
    	PostBet client = new PostBet(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				AppDialog dialogMess = new AppDialog(PlaceBetActivity.this, "Bet send to server.", "OK", null);
				dialogMess.setListener(new DialogListener() {
					
					@Override
					public void onOkClicked(Dialog d) {
						d.dismiss();
						finish();
					}
					
					@Override
					public void onCancelClicked(Dialog d) {}
				});
				dialogMess.show();
				dialog.dismiss();
				
				LocalBroadcastManager.getInstance(PlaceBetActivity.this).sendBroadcast(new Intent(Const.REFRESH_DATA_INTENT));
			}
			
			@Override
			public void onError(String errorMessage) {
				dialog.dismiss();
			}
		});
    	
    	client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				dialog.show();				
			}
			
			@Override
			public void onCancel() {
				
			}
		});
    	
    	client.runClient(getPreferences().getUuid(), bets, String.valueOf(round), String.valueOf(mAllCoef), 
    			String.valueOf(mMaxCoef), String.valueOf(mPlaceBet));
    }
    
    private ViewGroup getChildAt(ViewGroup layout){
    	return (ViewGroup) ((ViewGroup)layout.getChildAt(0)).getChildAt(0);
//    	return (ViewGroup)layout.getChildAt(0);
    }
    
    @Override
    public void onBackPressed() {
    	finishWithanimation(this);
    }
    
    class BetModelLocal{
    	private int matchId;
    	private int betType;
    	
		public BetModelLocal(int matchId, int betType) {
			this.matchId = matchId;
			this.betType = betType;
		}
		public int getMatchId() {
			return matchId;
		}
		public void setMatchId(int matchId) {
			this.matchId = matchId;
		}
		public int getBetType() {
			return betType;
		}
		public void setBetType(int betType) {
			this.betType = betType;
		}
    }
    
}
