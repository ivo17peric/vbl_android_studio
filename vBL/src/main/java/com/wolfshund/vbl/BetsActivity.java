package com.wolfshund.vbl;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.wolfshund.adapters.UserBetsAdapter;
import com.wolfshund.api.GetSingleBet;
import com.wolfshund.api.GetUserBets;
import com.wolfshund.api.model.BetData;
import com.wolfshund.api.model.BetInformation;
import com.wolfshund.api.model.UserBetsData;
import com.wolfshund.api.model.UserBetsInformation;
import com.wolfshund.dialog.LoadingDialog;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.listeners.ErrorApiListener;
import com.wolfshund.utils.Const;
import com.wolfshund.utils.Utils;

public class BetsActivity extends BaseActivity {
	
	private int mPage = 1;
	private int mTotalBets = 0;
	private UserBetsData mData;
	private BetData mBetData;
	private ListView mListView;
	
	public static void start(Context c){
		Intent intent = new Intent(c, BetsActivity.class);
		startActivityWithAnimation(c, intent);
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bets, R.id.parentBets);
        
        mListView = (ListView) findViewById(R.id.lvUsersBet);
        mListView.setAdapter(new UserBetsAdapter(this));
        ((UserBetsAdapter) mListView.getAdapter()).setData(new ArrayList<UserBetsInformation>());
        
        Button loadNext = (Button) findViewById(R.id.btnLoadNext);
        loadNext.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mListView.getAdapter().getCount() < mTotalBets){
					mPage++;
					getBets(mPage);
				}
			}
		});
        
        new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// wait till override animation end
				checkForTutorial(Const.Activites.BETSLIST, new BaseActivity.CheckTutorialListener() {
					
					@Override
					public void onTutorialCheckFinish() {
						getBets(mPage);
					}
				});
			}
		}, 300);
    }
    
    private void setData(){
    	TextView tvPoints = (TextView) findViewById(R.id.tvMyPPoints);
    	tvPoints.setText("Points: " + mData.getAroundData().getUserData().getpPoints());
    	
    	((UserBetsAdapter) mListView.getAdapter()).addData(mData.getAroundData().getUsersBetInformationList());
    	((UserBetsAdapter) mListView.getAdapter()).notifyDataSetChanged();
    	
    	mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				getBet((int) ((UserBetsAdapter)mListView.getAdapter()).getItemId(arg2));
			}
		});
    }
    
    private void setBetData(){
    	TextView placeBetTextView = (TextView) findViewById(R.id.placeBet);
    	placeBetTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (30 * k));
    	placeBetTextView.setText(String.valueOf(mBetData.getBetInformationList().get(0).getBetPlace()));
    	
    	TextView coeffTextView = (TextView) findViewById(R.id.coeff);
    	coeffTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (30 * k));
    	coeffTextView.setText(String.valueOf(mBetData.getBetInformationList().get(0).getKoef()));
    	
    	TextView hWinTextView = (TextView) findViewById(R.id.halfWin);
    	hWinTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (30 * k));
    	if(mBetData.getBetInformationList().size() < 5){
        	hWinTextView.setText("-");
    	}else{
        	hWinTextView.setText(String.valueOf(mBetData.getBetInformationList().get(0).getHalfPrice()));
    	}
    	
    	if(mBetData.getBetInformationList().get(0).getResult() == 3){
    		hWinTextView.setTextColor(getResources().getColor(R.color.win_green));
    	}else if(mBetData.getBetInformationList().get(0).getResult() == 2){
    		hWinTextView.setTextColor(getResources().getColor(R.color.lose_red));
    	}else if(mBetData.getBetInformationList().get(0).getResult() == 1){
    		hWinTextView.setTextColor(Color.GRAY);
    	}else{
    		hWinTextView.setTextColor(Color.WHITE);
    	}
    	
    	TextView winTextView = (TextView) findViewById(R.id.win);
    	winTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (30 * k));
    	winTextView.setText(String.valueOf(mBetData.getBetInformationList().get(0).getPrice()));
    	
    	if(mBetData.getBetInformationList().get(0).getResult() == 1){
    		winTextView.setTextColor(getResources().getColor(R.color.win_green));
    	}else if(mBetData.getBetInformationList().get(0).getResult() == 2 ||
    			mBetData.getBetInformationList().get(0).getResult() == 3){
    		winTextView.setTextColor(getResources().getColor(R.color.lose_red));
    	}else{
    		winTextView.setTextColor(Color.WHITE);
    	}
    	
    	LinearLayout layout = (LinearLayout) findViewById(R.id.llForBet);
    	layout.removeAllViews();
		int i = 1;
		
    	for(BetInformation item : mBetData.getBetInformationList()){
    		View view = LayoutInflater.from(this).inflate(R.layout.item_bet, layout, false);
			Utils.scaleToFit(this, view);
			
			TextView homeTeam = (TextView) view.findViewById(R.id.tvHomeTeam);
			homeTeam.setText(item.getHomeName());
			
			TextView awayTeam = (TextView) view.findViewById(R.id.tvAwayTeam);
			awayTeam.setText(item.getAwayName());
			
			TextView bet = (TextView) view.findViewById(R.id.tvBet);
			switch (item.getBet()) {
			case Const.BetType.HOME_WIN:
				bet.setText(Const.BetType.HOME_WIN_STRING);
				break;
				
			case Const.BetType.AWAY_WIN:
				bet.setText(Const.BetType.AWAY_WIN_STRING);
				break;
				
			case Const.BetType.DRAW:
				bet.setText(Const.BetType.DRAW_STRING);
				break;
				
			case Const.BetType.HOME_WIN_OR_DRAW:
				bet.setText(Const.BetType.HOME_WIN_OR_DRAW_STRING);
				break;
				
			case Const.BetType.AWAY_WIN_OR_DRAW:
				bet.setText(Const.BetType.AWAY_WIN_OR_DRAW_STRING);
				break;
				
			case Const.BetType.HOME_HENDICEP:
				bet.setText(Const.BetType.HOME_HENDICEP_STRING);
				break;
				
			case Const.BetType.AWAY_HENDICEP:
				bet.setText(Const.BetType.AWAY_HENDICEP_STRING);
				break;

			default:
				break;
			}
			
			TextView matchRes = (TextView) view.findViewById(R.id.tvMatchRes);
			if(item.getResult() == 0){
				matchRes.setText("-:-");
			}else{
				matchRes.setText(item.getHomeGoals()+":"+item.getAwayGoals());
			}
			
			View viewRes = view.findViewById(R.id.viewRes);
			if(item.getResult() == 0){
				viewRes.setBackgroundResource(R.drawable.no_result);
			}else if(isWinPair(item)){
				viewRes.setBackgroundResource(R.drawable.yes);
			}else{
				viewRes.setBackgroundResource(R.drawable.failed);
			}
			
			if(i % 2 == 0){
				view.setBackgroundColor(getResources().getColor(R.color.even_row));
			}else{
				view.setBackgroundColor(getResources().getColor(R.color.odd_row));
			}
			
			i++;
			
			layout.addView(view);
			
    	}
    }
    
    private boolean isWinPair(BetInformation item){
    	switch (item.getBet()) {
		case Const.BetType.HOME_WIN:
			if(item.getAwayGoals() < item.getHomeGoals()) return true;
			return false;
		case Const.BetType.AWAY_WIN:
			if(item.getAwayGoals() > item.getHomeGoals()) return true;
			return false;
		case Const.BetType.DRAW:
			if(item.getAwayGoals() == item.getHomeGoals()) return true;
			return false;
		case Const.BetType.HOME_WIN_OR_DRAW:
			if(item.getAwayGoals() <= item.getHomeGoals()) return true;
			return false;
		case Const.BetType.AWAY_WIN_OR_DRAW:
			if(item.getAwayGoals() >= item.getHomeGoals()) return true;
			return false;
		case Const.BetType.HOME_HENDICEP:
			if(item.getAwayGoals()+1 < item.getHomeGoals()) return true;
			return false;
		case Const.BetType.AWAY_HENDICEP:
			if(item.getAwayGoals() > item.getHomeGoals()+1) return true;
			return false;
    	}
    	return false;
    }
    
    private void getBets(int page){
    	final LoadingDialog dialog = new LoadingDialog(this);
    	dialog.show();
    	
    	GetUserBets client = new GetUserBets(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				mData = (UserBetsData) result;
				mTotalBets = mData.getAroundData().getNumOfBet();
				setData();
				dialog.dismiss();
				if(mData.getAroundData().getUsersBetInformationList().size() > 0)
					getBet(mData.getAroundData().getUsersBetInformationList().get(0).getId());
			}
			
			@Override
			public void onError(String errorMessage) {
				dialog.dismiss();
			}
		});
    	
    	client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				dialog.show();
			}
			
			@Override
			public void onCancel() {
				finish();
			}
		});
    	
    	client.runClient(getPreferences().getUuid(), page);
    }
    
    private void getBet(int id){
    	final LoadingDialog dialog = new LoadingDialog(this);
    	dialog.show();
    	
    	GetSingleBet client = new GetSingleBet(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				mBetData = (BetData) result;
				setBetData();
				dialog.dismiss();
			}
			
			@Override
			public void onError(String errorMessage) {
				dialog.dismiss();
			}
		});
    	
    	client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				dialog.show();
			}
			
			@Override
			public void onCancel() {
				finish();
			}
		});
    	
    	client.runClient(id);
    }
    
    @Override
    public void onBackPressed() {
    	finishWithanimation(this);
    }
    
}
