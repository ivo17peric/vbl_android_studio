package com.wolfshund.vbl;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wolfshund.api.model.LeagueTableInformation;
import com.wolfshund.api.model.MasterData;
import com.wolfshund.utils.Const;
import com.wolfshund.utils.Utils;

public class TableFragment extends BaseFragment{
	
	private MasterData mData;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view = setView(R.layout.fragment_table, R.id.parentTableFragment, inflater, container);
		
		view.findViewById(R.id.viewClickableArea).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (getActivity() instanceof HomeActivity) {
					((HomeActivity) getActivity()).startNextActivity(Const.Home.TABLE);
				}
			}
		});
		
		return view;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mData = (MasterData) getArguments().getSerializable(Const.Extra.MASTER);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		LinearLayout layout = (LinearLayout) getView().findViewById(R.id.llInScrollViewTable);
		
		int i = 1;
		for(LeagueTableInformation item : mData.getMasterInformation().getTable()){
			View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_table, layout, false);
			Utils.scaleToFit(getActivity(), view);
			
			TextView tvName = (TextView) view.findViewById(R.id.tvTeamShortName);
			tvName.setText(item.getShortName());
			
			TextView tvGamePlayed = (TextView) view.findViewById(R.id.tvGamePlayed);
			tvGamePlayed.setText(String.valueOf(item.getGames()));
			
			TextView tvWinGames = (TextView) view.findViewById(R.id.tvWinGames);
			tvWinGames.setText(String.valueOf(item.getWinGames()));
			
			TextView tvDrawGames = (TextView) view.findViewById(R.id.tvDrawGames);
			tvDrawGames.setText(String.valueOf(item.getDrawGames()));
			
			TextView tvLoseGames = (TextView) view.findViewById(R.id.tvLoseGames);
			tvLoseGames.setText(String.valueOf(item.getLoseGames()));
			
			TextView tvGoalDif = (TextView) view.findViewById(R.id.tvGoalDiference);
			tvGoalDif.setText(String.valueOf((item.getGoalsScored() - item.getGoalsAllowed())));
			
			TextView tvPoints = (TextView) view.findViewById(R.id.tvPoints);
			tvPoints.setText(String.valueOf(item.getPoints()));
			
			if(i % 2 == 0){
				view.setBackgroundColor(getResources().getColor(R.color.even_row));
			}else{
				view.setBackgroundColor(getResources().getColor(R.color.odd_row));
			}
			
			i++;
			
			layout.addView(view);
		}
	}
	
}
