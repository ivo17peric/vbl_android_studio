package com.wolfshund.vbl;

import android.os.Bundle;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wolfshund.adapters.RoundPagerAdapter;
import com.wolfshund.api.model.MasterData;
import com.wolfshund.utils.Const;
import com.wolfshund.utils.LogCS;
import com.wolfshund.view.ViewPagerWithDisabling;

public class RoundFragment extends BaseFragment{
	
	private static final int LEFT = 0;
	private static final int RIGHT = 2;
	
	private MasterData mData;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view = setView(R.layout.fragment_round, R.id.parentRoundFragment, inflater, container);
		
		mData = (MasterData) getArguments().getSerializable(Const.Extra.MASTER);
		
		final ViewPagerWithDisabling pager = (ViewPagerWithDisabling) view.findViewById(R.id.roundPager);
		pager.setPagingEnabled(true);
		RoundPagerAdapter adapter = new RoundPagerAdapter(getFragmentManager(), mData);
		pager.setAdapter(adapter);
		pager.setCurrentItem(1);
		
		if(mData.getMasterInformation().getCurrentRound() != null && mData.getMasterInformation().getCurrentRound().size() > 0){
			((HomeActivity)getActivity()).setSelectedRound(mData.getMasterInformation().getCurrentRound().get(0).getRound());
		}
		
		final View left = view.findViewById(R.id.viewPagerLeft);
		final View right = view.findViewById(R.id.viewPagerRight);
		
		right.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(pager.getCurrentItem()+1 <= 2){
					pager.setCurrentItem(pager.getCurrentItem()+1);
				}
			}
		});
		
		left.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(pager.getCurrentItem()-1 >= 0){
					pager.setCurrentItem(pager.getCurrentItem()-1);
				}
			}
		});
		
		pager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				manageArrows(arg0, left, right);
				manageHome(arg0);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {}
		});
		
		return view;
	}
	
	private void manageArrows(int type, View left, View right){
		if(type == LEFT){
			left.setVisibility(View.INVISIBLE);
		}else if(type == RIGHT){
			right.setVisibility(View.INVISIBLE);
		}else{
			left.setVisibility(View.VISIBLE);
			right.setVisibility(View.VISIBLE);
		}
	}
	
	private void manageHome(int pos){
		if(getActivity() instanceof HomeActivity){
			try {
				switch (pos) {
				case 0:
					((HomeActivity)getActivity()).setSelectedRound(mData.getMasterInformation().getPrevRound().get(0).getRound());
					break;
				case 1:
					((HomeActivity)getActivity()).setSelectedRound(mData.getMasterInformation().getCurrentRound().get(0).getRound());
					break;
				case 2:
					((HomeActivity)getActivity()).setSelectedRound(mData.getMasterInformation().getNextRound().get(0).getRound());
					break;

				default:
					break;
				}
			} catch (Exception e) {
				LogCS.e("LOG", "THERE IS NO ROUNDS DEFINED");
				e.printStackTrace();
			}
			
		}
	}
	
}
