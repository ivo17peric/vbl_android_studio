package com.wolfshund.vbl;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.wolfshund.adapters.UsersAdvanceAdapter;
import com.wolfshund.api.GetTopAndAround;
import com.wolfshund.api.model.TopAndAroundData;
import com.wolfshund.api.model.UserInformation;
import com.wolfshund.api.model.UsersData;
import com.wolfshund.dialog.LoadingDialog;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.listeners.ErrorApiListener;
import com.wolfshund.utils.Const;

public class UsersActivity extends BaseActivity implements OnClickListener{
	
	private TopAndAroundData mData;
	private Button btnUsersPoints;
	private Button btnTenGamesPoints;
	private Button btnAllTimePoints;
	private int mSelectedUsers;
	
	private ListView listviewAround;
	private ListView listviewTop;
	
	private int currentPage = 1;
	private int currentAroundPageBelow = 1;
	private int currentAroundPageAbove = 1;
	
	public static void start(Context c, int selectedUsers){
		Intent intent = new Intent(c, UsersActivity.class);
		intent.putExtra(Const.Extra.SELECTED_USERS, selectedUsers);
		startActivityWithAnimation(c, intent);
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users, R.id.parentUsers);
        
        btnUsersPoints = (Button) findViewById(R.id.btnUserPoints);
        btnTenGamesPoints = (Button) findViewById(R.id.btnTenGamesPoints);
        btnAllTimePoints = (Button) findViewById(R.id.btnAllTimePoints);
        btnUsersPoints.setOnClickListener(this);
        btnAllTimePoints.setOnClickListener(this);
        btnTenGamesPoints.setOnClickListener(this);
        
        
        listviewAround = (ListView) findViewById(R.id.listviewAround);
        listviewTop = (ListView) findViewById(R.id.listviewTop);
        
        mSelectedUsers = getIntent().getIntExtra(Const.Extra.SELECTED_USERS, UsersFragment.SELECTED_LAST_TEN);
        setSelectedButtons();
        
        new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// wait till override animation end
				checkForTutorial(Const.Activites.USERSLIST, new BaseActivity.CheckTutorialListener() {
					
					@Override
					public void onTutorialCheckFinish() {
						getUsers();
					}
				});
			}
		}, 300);
    }
    
    private void setSelectedButtons(){
    	switch (mSelectedUsers) {
		case UsersFragment.SELECTED_P_POINTS:
			btnAllTimePoints.setSelected(false);
			btnTenGamesPoints.setSelected(false);
			btnUsersPoints.setSelected(true);
			break;
			
		case UsersFragment.SELECTED_POINTS:
			btnAllTimePoints.setSelected(true);
			btnTenGamesPoints.setSelected(false);
			btnUsersPoints.setSelected(false);
			break;

		case UsersFragment.SELECTED_LAST_TEN:
			btnAllTimePoints.setSelected(false);
			btnTenGamesPoints.setSelected(true);
			btnUsersPoints.setSelected(false);
			break;

		default:
			break;
		}
    }
    
    private void setData(){
    	UsersAdvanceAdapter aroundAdapter = new UsersAdvanceAdapter(this);
    	aroundAdapter.setData(mData.getTopAndAround().getAroundList());
    	aroundAdapter.setType(mSelectedUsers, Const.ListType.AROUND);
    	listviewAround.setAdapter(aroundAdapter);
    	listviewAround.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				Log.d("LOG", listviewAround.getAdapter().getCount()+ " : " + position);
				if(position == listviewAround.getAdapter().getCount() - 1){
					currentAroundPageBelow++;
					getNextUsers(true);
				}else if(position == 0){
					currentAroundPageAbove++;
					getNextUsers(false);
				}else{
					Toast.makeText(UsersActivity.this, ((UserInformation)listviewAround.getAdapter().getItem(position)).getUsername(), 3000).show();
				}
			}
		});
    	
    	//find my place for set current item
    	int myListPosition = 0;
    	int myPosition = 0;
    	for(int i = 0; i < mData.getTopAndAround().getAroundList().size(); i++){
    		if(mData.getTopAndAround().getAroundList().get(i).getUuid().equals(getPreferences().getUuid())){
    			myListPosition = i;
    			myPosition = mData.getTopAndAround().getAroundList().get(i).getPosMy();
    		}
    	}
    	//set other users positions
    	int j = 1;
    	for(int i = myListPosition-1; i >= 0; i--){
    		mData.getTopAndAround().getAroundList().get(i).setPosMy(myPosition - j);
    		j++;
    	}
    	j = 1;
    	for(int i = myListPosition+1; i < mData.getTopAndAround().getAroundList().size(); i++){
    		mData.getTopAndAround().getAroundList().get(i).setPosMy(myPosition + j);
    		j++;
    	}
    	listviewAround.setSelection(myListPosition);
    	
    	UsersAdvanceAdapter topAdapter = new UsersAdvanceAdapter(this);
    	topAdapter.setData(mData.getTopAndAround().getTopList());
    	topAdapter.setType(mSelectedUsers, Const.ListType.TOP);
    	listviewTop.setAdapter(topAdapter);
    	listviewTop.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				if(position == listviewTop.getAdapter().getCount() - 1){
					currentPage++;
					getNextUsers();
				}else{
					Toast.makeText(UsersActivity.this, ((UserInformation)listviewTop.getAdapter().getItem(position)).getUsername(), 3000).show();
				}
			}
		});
    }
    
    private void addData(int listType, List<UserInformation> list, boolean isBelow){
    	
    	if(listType == Const.ListType.TOP){
    		((UsersAdvanceAdapter)listviewTop.getAdapter()).addData(list);
    	}else{
    		if(isBelow){
    			int lastPosition = mData.getTopAndAround().getAroundList().get(mData.getTopAndAround().getAroundList().size() - 1).getPosMy();
    			for(int i = 0; i < list.size(); i++){
    				list.get(i).setPosMy(lastPosition + i + 1);
    			}
    			((UsersAdvanceAdapter)listviewAround.getAdapter()).addData(list);
    		}else{
    			int lastPosition = mData.getTopAndAround().getAroundList().get(0).getPosMy();
    			int j = 1;
    			for(int i = list.size() - 1; i >= 0; i--){
    				list.get(i).setPosMy(lastPosition - j );
    				j++;
    			}
    			((UsersAdvanceAdapter)listviewAround.getAdapter()).addDataToTop(list);
    			listviewAround.setSelection(list.size());
    		}
    	}
    }
    
    @Override
	public void onClick(View v) {
    	switch (v.getId()) {
		case R.id.btnUserPoints:
			mSelectedUsers = UsersFragment.SELECTED_P_POINTS;
			break;
			
		case R.id.btnTenGamesPoints:
			mSelectedUsers = UsersFragment.SELECTED_LAST_TEN;
			break;
			
		case R.id.btnAllTimePoints:
			mSelectedUsers = UsersFragment.SELECTED_POINTS;
			break;

		default:
			break;
		}
    	
    	switch (v.getId()) {
		case R.id.btnUserPoints:
		case R.id.btnTenGamesPoints:
		case R.id.btnAllTimePoints:
			setResetPages();
			setSelectedButtons();
			getUsers();
			break;

		default:
			break;
		}
	}
    
    private void setResetPages() {
    	currentPage = 1;
    	currentAroundPageAbove = 1;
    	currentAroundPageBelow = 1;
	}

	private void getUsers(){
    	final LoadingDialog dialog = new LoadingDialog(this);
    	dialog.show();
    	
    	GetTopAndAround client = new GetTopAndAround(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				mData = ((TopAndAroundData)result);
				setData();
				dialog.dismiss();
			}
			
			@Override
			public void onError(String errorMessage) {
				dialog.dismiss();
			}
		});
    	
    	client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				dialog.show();
			}
			
			@Override
			public void onCancel() {
				finishWithanimation(UsersActivity.this);
			}
		});
    	
    	String type = Const.TopAndAround.POINTS_LAST_TEN;
    	if(mSelectedUsers == UsersFragment.SELECTED_LAST_TEN){
    		type = Const.TopAndAround.POINTS_LAST_TEN;
    	}else if(mSelectedUsers == UsersFragment.SELECTED_P_POINTS){
    		type = Const.TopAndAround.P_POINTS_USERS_POINTS;
    	}else if(mSelectedUsers == UsersFragment.SELECTED_POINTS){
    		type = Const.TopAndAround.POINTS_ALL_TIME;
    	}
    	
    	client.runClient(getPreferences().getUuid(), type);
    }
    
    private void getNextUsers(){
    	final LoadingDialog dialog = new LoadingDialog(this);
    	dialog.show();
    	
    	GetTopAndAround client = new GetTopAndAround(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				UsersData data = ((UsersData)result);
				addData(Const.ListType.TOP, data.getUsers(), false);
				dialog.dismiss();
			}
			
			@Override
			public void onError(String errorMessage) {
				currentPage--;
				dialog.dismiss();
			}
		});
    	
    	client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				currentPage++;
				dialog.show();
			}
			
			@Override
			public void onCancel() {
				finishWithanimation(UsersActivity.this);
			}
		});
    	
    	String type = Const.TopAndAround.POINTS_LAST_TEN;
    	if(mSelectedUsers == UsersFragment.SELECTED_LAST_TEN){
    		type = Const.TopAndAround.POINTS_LAST_TEN;
    	}else if(mSelectedUsers == UsersFragment.SELECTED_P_POINTS){
    		type = Const.TopAndAround.P_POINTS_USERS_POINTS;
    	}else if(mSelectedUsers == UsersFragment.SELECTED_POINTS){
    		type = Const.TopAndAround.POINTS_ALL_TIME;
    	}
    	
    	client.runClient(getPreferences().getUuid(), type, currentPage);
    }
    
    private void getNextUsers(final boolean isBelow){
    	final LoadingDialog dialog = new LoadingDialog(this);
    	dialog.show();
    	
    	GetTopAndAround client = new GetTopAndAround(this, new ApiListener() {
			
			@Override
			public void onSuccess(Object result) {
				UsersData data = ((UsersData)result);
				addData(Const.ListType.AROUND, data.getUsers(), isBelow);
				dialog.dismiss();
			}
			
			@Override
			public void onError(String errorMessage) {
				if(isBelow) currentAroundPageBelow--;
				else currentAroundPageAbove--;
				dialog.dismiss();
			}
		});
    	
    	client.setErrorListener(new ErrorApiListener() {
			
			@Override
			public void onRetry() {
				if(isBelow) currentAroundPageBelow++;
				else currentAroundPageAbove++;
				dialog.show();
			}
			
			@Override
			public void onCancel() {
				finishWithanimation(UsersActivity.this);
			}
		});
    	
    	String type = Const.TopAndAround.POINTS_LAST_TEN;
    	if(mSelectedUsers == UsersFragment.SELECTED_LAST_TEN){
    		type = Const.TopAndAround.POINTS_LAST_TEN;
    	}else if(mSelectedUsers == UsersFragment.SELECTED_P_POINTS){
    		type = Const.TopAndAround.P_POINTS_USERS_POINTS;
    	}else if(mSelectedUsers == UsersFragment.SELECTED_POINTS){
    		type = Const.TopAndAround.POINTS_ALL_TIME;
    	}
    	
    	int page = 1;
    	if(isBelow) page = currentAroundPageBelow;
		else page = currentAroundPageAbove;
    	
    	client.runClient(getPreferences().getUuid(), type, page, isBelow);
    }
    
    @Override
    public void onBackPressed() {
    	finishWithanimation(this);
    }

}
