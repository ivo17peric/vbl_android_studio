package com.wolfshund.utils;

import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.cloverstudio.layouthelper.LayoutHelper;
import com.cloverstudio.layouthelper.LayoutHelperFactory;
import com.cloverstudio.layouthelper.Resolution;
import com.wolfshund.vbl.R;

public class Utils {

	private static float k = 1.0f;

	private static final LayoutHelper sLayoutHelper = LayoutHelperFactory
			.createLandscapeLayoutHelper(Resolution.XHDPI);
	
	public static void initializeK(Activity activity) {
		if (k == 1.0f) {
			k = getLayoutHelper().getK(activity);
		}
	}
	
	public static LayoutHelper getLayoutHelper() {
		return sLayoutHelper;
	}
	
	public static float getK() {
		return k;
	}

	public static void scaleToFit(Activity activity, View viewToScale) {
		sLayoutHelper.scaleToFit(viewToScale, activity);
	}
	
	public static String generateUUID(long ms){
		StringBuilder sb = new StringBuilder();
		sb.append(ms);
		sb.append(getRadnomString(15));
		return sb.toString();
	}
	
	public static String getRadnomString(int lenght){
        Random random=new Random();
		String chars="0123456789qwertzuioplkjhgfdsayxcvbnmQWERTZUIOPLKJHGFDSAYXCVBNM";
        StringBuilder stringBuilder=new StringBuilder();
        for(int i=0;i<lenght;i++){
            stringBuilder.append(chars.charAt(random.nextInt(chars.length())));
        }
        return stringBuilder.toString();
	}
	
	public static String getRadnomNumber(int lenght){
        Random random=new Random();
		String chars="0123456789";
        StringBuilder stringBuilder=new StringBuilder();
        for(int i=0;i<lenght;i++){
            stringBuilder.append(chars.charAt(random.nextInt(chars.length())));
        }
        return stringBuilder.toString();
	}
	
	public static int getBetSize(String bet){
		String[] bets = bet.split(";");
		return bets.length;
	}
	
	public static void populateLastFive(Context c, LinearLayout layout, String lastFive, float k){
		for(int z = 0; z < lastFive.length(); z++){
			View round = new View(c);
			layout.addView(round);
			round.getLayoutParams().width = (int) (20 * k);
			round.getLayoutParams().height = (int) (20 * k);
			((LayoutParams)round.getLayoutParams()).rightMargin = (int) (2 * k);
			if(lastFive.charAt(z) == '1'){
				round.setBackgroundResource(R.drawable.round_won);
			}else if(lastFive.charAt(z) == '2'){
				round.setBackgroundResource(R.drawable.round_lost);
			}else if(lastFive.charAt(z) == '3'){
				round.setBackgroundResource(R.drawable.round_drawn);
			}else{
				round.getLayoutParams().width = (int) (10 * k);
				round.getLayoutParams().height = (int) (5 * k);
				((LayoutParams)round.getLayoutParams()).rightMargin = (int) (7 * k);
				((LayoutParams)round.getLayoutParams()).leftMargin = (int) (5 * k);
				round.setBackgroundResource(R.drawable.round_not_played);
			}
		}
    }
	
}
