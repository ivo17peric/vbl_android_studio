package com.wolfshund.utils;

import android.util.Log;

/**
 * @author Josip Marković
 */
public class LogCS {

    /**
     * This flag has be set to TRUE in order to display logs.
     * In current implementation, flag is set to Const.LOG
     */
    private static boolean DO_PRINT_FLAG = Const.LOG;

    public static void log(String level, String tag, String message) {
        if (!DO_PRINT_FLAG) return;
        if (level == null) return;
        if (tag == null) return;
        if (message == null) return;

        if (level.equals("d")) {
            Log.d(tag, message);
        } else if (level.equals("i")) {
            Log.i(tag, message);
        } else if (level.equals("e")) {
            Log.e(tag, message);
        } else if (level.equals("w")) {
            Log.w(tag, message);
        } else if (level.equals("v")) {
            Log.v(tag, message);
        } else if (level.equals("wtf")) {
            Log.wtf(tag, message);
        }
    }

    public static void d(String tag, String message) {
        log("d", tag, message);
    }

    public static void i(String tag, String message) {
        log("i", tag, message);
    }

    public static void e(String tag, String message) {
        log("e", tag, message);
    }

    public static void w(String tag, String message) {
        log("w", tag, message);
    }

    public static void v(String tag, String message) {
        log("v", tag, message);
    }

    public static void wtf(String tag, String message) {
        log("wtf", tag, message);
    }
}
