package com.wolfshund.utils;

public class Const {
	
	public static final boolean LOG = true;
	
	public static final String REFRESH_DATA_INTENT = "REFRESHDATA";
	
	public static final class BaseModel {
		public static final String RESULT = "result";
		public static final String MESSAGE = "message";
		public static final String DATA = "data";
	}
	
	public static final class Home {
		public static final int ROUND = 1;
		public static final int BETS = 2;
		public static final int USERS = 3;
		public static final int TABLE = 4;
		public static final int USER_DATA = 5;
		public static final int NONE = 99;
	}
	
	public static final class UserDataModel {
		public static final String ID = "id";
		public static final String UUID = "uuid";
		public static final String EMAIL = "email";
		public static final String IS_VIP = "is_vip";
		public static final String VIP_TILL = "vip_till";
		public static final String LAST_LOGIN = "last_login";
		public static final String DEVICE_ID = "device_id";
		public static final String FIRST_LOGIN = "first_login";
		public static final String P_POINTS = "p_points";
		public static final String POINTS = "points";
		public static final String POINTS_TEN_GAMES = "points_ten_games";
		public static final String POINTS_TEN_GAMES_LAST = "points_ten_games_last";
		public static final String USERNAME = "username";
		
		public static final String USER_ID = "user_id";
		public static final String POS_MY = "pos_my";
		
		public static final String POS_USER_P_POINTS = "user_p_points_pos";
		public static final String POS_USER_POINTS = "user_points_pos";
		public static final String POS_LAST_TEN_GAMES = "user_last_ten_games_pos";
		public static final String POST_TEN_GAMES = "user_ten_games_pos";
	}
	
	public static final class LeagueTableModel {
		public static final String ID = "id";
		public static final String CLUB_ID = "club_id";
		public static final String POINTS = "points";
		public static final String WIN_GAMES = "win_games";
		public static final String DRAW_GAMES = "draw_games";
		public static final String LOSE_GAMES = "lose_games";
		public static final String GAMES = "games";
		public static final String GOALS_SCORED = "goals_scored";
		public static final String GOALS_ALLOWED = "goals_allowed";
		public static final String ID_SCHEDULE = "id_schedule";
		public static final String NAME = "name";
		public static final String SHORT_NAME = "short_name";
		public static final String OFFENCE = "offence";
		public static final String DEFENCE = "defence";
		public static final String INDEX_QUALITY = "index_quality";
		public static final String FORM = "form";
		public static final String LAST_FIVE_FORM = "last_five_form";
	}
	
	public static final class SingleBetModel {
		public static final String HOME_NAME = "home_name";
		public static final String AWAY_NAME = "away_name";
		public static final String BET = "bet";
		public static final String RESULT = "result";
		public static final String HOME_GOALS = "home_goals";
		public static final String AWAY_GOALS = "away_goals";
		public static final String BET_PLACE = "bet_place";
		public static final String KOEF = "koef";
		public static final String PRICE = "price";
		public static final String HALF_PRICE = "half_price";
		
		public static final String BET_ID = "bet_id";
	}
	
	public static final class BetRoundModel {
		public static final String SEASON = "season";
		public static final String ROUND = "round";
		public static final String HOME_NAME = "home_name";
		public static final String AWAY_NAME = "away_name";
		public static final String BETS = "bets";
		public static final String MATCH_ID = "match_id";
		
		public static final String HOME_WIN = "home_win";
		public static final String DRAW = "draw";
		public static final String AWAY_WIN = "away_win";
		public static final String HOME_DRAW_WIN = "home_draw_win";
		public static final String AWAY_DRAW_WIN = "away_draw_win";
		public static final String HOME_HENDICAP = "home_hendicap";
		public static final String AWAY_HENDICAP = "away_hendicap";
		
		public static final String OFFSET = "offset";
		public static final String BET_NUM = "bet_num";
	}
	
	public static final class RoundModel {
		public static final String HOME = "home";
		public static final String HOME_OFFENCE = "home_offence";
		public static final String HOME_DEFENCE = "home_defence";
		public static final String HOME_LAST_FIVE_FORM = "home_last_form";
		public static final String HOME_FORM = "home_form";
		public static final String AWAY = "away";
		public static final String AWAY_OFFENCE = "away_offence";
		public static final String AWAY_DEFENCE = "away_defence";
		public static final String AWAY_LAST_FIVE_FORM = "away_last_form";
		public static final String AWAY_FORM = "away_form";
		public static final String ID = "id";
		public static final String ROUND = "round";
		public static final String SEASON = "season";
		public static final String HOME_GOALS = "home_goals";
		public static final String AWAY_GOALS = "away_goals";
		public static final String DATE_MATCH = "date_match";
		
		public static final String OFFSET = "offset";
	}
	
	public static final class UserBetsModel {
		public static final String ID = "id";
		public static final String BET_PLACE = "bet_place";
		public static final String SEASON = "season";
		public static final String ROUND = "round";
		public static final String USER_ID = "user_id";
		public static final String BET = "bet";
		public static final String KOEF = "koef";
		public static final String MAX_KOEF = "max_koef";
		public static final String RESULT = "result";
	}
	
	public static final class MasterDataModel {
		public static final String CURRENT_ROUND = "current_round";
		public static final String NEXT_ROUND = "next_round";
		public static final String PREV_ROUND = "prev_round";
		public static final String TABLE = "table";
		public static final String USER = "user";
		public static final String BETS = "bets";
		public static final String USERS_P_POINTS = "users_p_points";
		public static final String USERS_POINTS = "users_points";
		public static final String USERS_POINTS_TEN_GAMES = "users_ten_games";
		
		public static final String USER_ID = "user_id";
		public static final String UUID = "uuid";
	}
	
	public static final class TopAndAround {
		public static final String TOP = "top";
		public static final String AROUND = "around";
		public static final String ORDER_BY = "orderBy";
		public static final String TO_BELOW = "to_below";
		
		public static final String P_POINTS_USERS_POINTS = "p_points";
		public static final String POINTS_ALL_TIME = "points";
		public static final String POINTS_LAST_TEN = "points_ten_games";
	}
	
	public static final class PostUser {
		public static final String UUID = "uuid";
		public static final String EMAIL = "email";
		public static final String DEVICE_ID = "deviceId";
		public static final String USERNAME = "username";
	}
	
	public static final class PostBet {
		public static final String UUID = "uuid";
		public static final String ROUND = "round";
		public static final String COEF = "coef";
		public static final String BET = "bet";
		public static final String MAX_COEF = "max_coef";
		public static final String PLACE_BET = "place_bet";
	}
	
	public static final class Error {
		public static final String ERROR_MESSAGE = "error_message";
		public static final String DEFAULT_MESSAGE = "Connection Error!!!";
	}
	
	public static final class ApiUrl {
		public static final String API = "http://infobettips.com/spec/api/";
		public static final String USER_DATA = API + "get_user_data.php";
		public static final String SINGLE_BET = API + "get_bet.php";
		public static final String ROUND_FOR_BET = API + "get_round_for_bet.php";
		public static final String USER_BETS = API + "get_user_bets.php";
		public static final String MASTER_DATA = API + "get_master_data.php";
		public static final String ROUND = API + "get_round.php";
		public static final String CREATE_USER = API + "create_user.php";
		public static final String LEAGUE_TABLE = API + "get_league_table.php?get=1";
		public static final String SEND_BET = API + "place_bet.php";
		public static final String TOP_AND_AROUND = API + "get_users_and_around_users.php";
		public static final String GET_USERS = API + "get_users.php";
		public static final String GET_AROUND_USERS = API + "get_arround_user.php";
		public static final String GET_USER_DATA_EXT = API + "get_user_data_with_pos.php";
		public static final String EXPORT_USER = API + "export_user.php";
		public static final String IMPORT_USER = API + "import_user.php";
	}
	
	public static final class Extra {
		public static final String PAGE = "page";
		public static final String TOTAL_PAGE = "total_page";
		public static final String ROUND = "round";
		public static final String MASTER = "master";
		public static final String SELECTED_USERS = "selected_users";
	}
	
	public static final class BetType {
		public static final int HOME_WIN = 1;
		public static final int DRAW = 2;
		public static final int AWAY_WIN = 3;
		public static final int HOME_WIN_OR_DRAW = 4;
		public static final int AWAY_WIN_OR_DRAW = 5;
		public static final int HOME_HENDICEP = 6;
		public static final int AWAY_HENDICEP = 7;
		
		public static final String HOME_WIN_STRING = "1";
		public static final String DRAW_STRING = "X";
		public static final String AWAY_WIN_STRING = "2";
		public static final String HOME_WIN_OR_DRAW_STRING = "1X";
		public static final String AWAY_WIN_OR_DRAW_STRING = "X2";
		public static final String HOME_HENDICEP_STRING = "H(0:1)";
		public static final String AWAY_HENDICEP_STRING = "H(1:0)";
	}
	
	public static final class ListType {
		public static final int TOP = 1;
		public static final int AROUND = 2;
	}
	
	public static final class ExportUser {
		public static final String PASSWORD = "password";
	}
	
	public static final class Activites {
		public static final String HOME = "HOME";
		public static final String USERDATA = "USERDATA";
		public static final String ROUNDLIST = "ROUNDLIST";
		public static final String BETSLIST = "BETSLIST";
		public static final String USERSLIST = "USERSLIST";
		public static final String TABLE = "TABLE";
		public static final String PLACEBET = "PLACEBET";
	}

}
