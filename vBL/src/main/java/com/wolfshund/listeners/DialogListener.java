package com.wolfshund.listeners;

import android.app.Dialog;

public interface DialogListener {
	public void onOkClicked(Dialog d);
    public void onCancelClicked(Dialog d);
}
