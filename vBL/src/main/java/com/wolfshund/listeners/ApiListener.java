package com.wolfshund.listeners;

public interface ApiListener {
	public void onSuccess(Object result);
    public void onError(String errorMessage);
}
