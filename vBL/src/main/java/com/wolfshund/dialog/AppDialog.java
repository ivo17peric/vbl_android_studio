package com.wolfshund.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.wolfshund.listeners.DialogListener;
import com.wolfshund.vbl.R;

public class AppDialog extends BaseDialog{
	
	private DialogListener mListener;
	private TextView mTvMessage;
	private Button mOk;
	private Button mCancel;
	
	public AppDialog(Context context, String message, String okString, String cancelString) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		
		setContentView(R.layout.dialog_app, R.id.parent_dialog);
		setCancelable(false);
		
		mTvMessage = (TextView) findViewById(R.id.tvMessage);
		mOk = (Button) findViewById(R.id.btnOk);
		mCancel = (Button) findViewById(R.id.btnCancel);
		
		mTvMessage.setText(message);
		
		if(!TextUtils.isEmpty(okString)){
			mOk.setText(okString);
		}
		mOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mListener != null){
					mListener.onOkClicked(AppDialog.this);
				}else{
					AppDialog.this.dismiss();
				}
			}
		});
		
		if(!TextUtils.isEmpty(cancelString)){
			mCancel.setText(cancelString);
			mCancel.setVisibility(View.VISIBLE);
		}
		mCancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mListener != null){
					mListener.onCancelClicked(AppDialog.this);
				}
			}
		});
		
	}
	
	public void setListener(DialogListener lis){
		mListener = lis;
	}
	
}
