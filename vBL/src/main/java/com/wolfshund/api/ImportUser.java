package com.wolfshund.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;

import com.wolfshund.api.model.ApiResponse;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.utils.Const;

public class ImportUser extends Client{
	
	private static String API_URL = Const.ApiUrl.IMPORT_USER;

	public ImportUser(Activity activity, ApiListener listener){
		super(activity);
		setApiListener(listener);
	}
	
	public void runClient(String uuid, String password, String userId, String deviceId){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(Const.PostBet.UUID, uuid));
		params.add(new BasicNameValuePair(Const.ExportUser.PASSWORD, password));
		params.add(new BasicNameValuePair(Const.UserDataModel.USER_ID, userId));
		params.add(new BasicNameValuePair(Const.UserDataModel.DEVICE_ID, deviceId));
		
		runPostClient(API_URL, ApiResponse.class, params);
	}
}
