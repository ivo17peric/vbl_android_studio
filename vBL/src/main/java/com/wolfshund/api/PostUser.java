package com.wolfshund.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;

import com.wolfshund.api.model.ApiResponse;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.utils.Const;

public class PostUser extends Client{
	
	private static String API_URL = Const.ApiUrl.CREATE_USER;

	public PostUser(Activity activity, ApiListener listener){
		super(activity);
		setApiListener(listener);
	}
	
	public void runClient(String uuid, String email, String username, String deviceId){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(Const.PostUser.UUID, uuid));
		params.add(new BasicNameValuePair(Const.PostUser.EMAIL, email));
		params.add(new BasicNameValuePair(Const.PostUser.USERNAME, username));
		params.add(new BasicNameValuePair(Const.PostUser.DEVICE_ID, deviceId));
		
		runPostClient(API_URL, ApiResponse.class, params);
	}
}
