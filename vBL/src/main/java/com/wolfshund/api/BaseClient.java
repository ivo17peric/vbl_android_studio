package com.wolfshund.api;

import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

public abstract class BaseClient {
	
	protected abstract JSONObject getJsonObject(String url);
	
	protected abstract JSONObject getJsonObject(String url, List<NameValuePair> params);
	
	public JSONObject getJsonResponse(String url){
		return getJsonResponse(url);
	}
	
	public JSONObject getJsonResponse(String url, List<NameValuePair> params){
		return getJsonResponse(url, params);
	}
	
}
