package com.wolfshund.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;

import com.wolfshund.api.model.RoundData;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.utils.Const;

public class GetRound extends Client{
	
	private static String API_URL = Const.ApiUrl.ROUND;

	public GetRound(Activity activity, ApiListener listener){
		super(activity);
		setApiListener(listener);
	}
	
	public void runClient(int roundId, int offset){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		if(roundId != -1) params.add(new BasicNameValuePair(Const.RoundModel.ROUND, String.valueOf(roundId)));
		if(offset != -1) params.add(new BasicNameValuePair(Const.RoundModel.OFFSET, String.valueOf(offset)));
		
		runPostClient(API_URL, RoundData.class, params);
	}
}
