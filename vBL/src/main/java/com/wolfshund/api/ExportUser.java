package com.wolfshund.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;

import com.wolfshund.api.model.ApiResponse;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.utils.Const;

public class ExportUser extends Client{
	
	private static String API_URL = Const.ApiUrl.EXPORT_USER;

	public ExportUser(Activity activity, ApiListener listener){
		super(activity);
		setApiListener(listener);
	}
	
	public void runClient(String uuid, String password){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(Const.PostBet.UUID, uuid));
		params.add(new BasicNameValuePair(Const.ExportUser.PASSWORD, password));
		
		runPostClient(API_URL, ApiResponse.class, params);
	}
}
