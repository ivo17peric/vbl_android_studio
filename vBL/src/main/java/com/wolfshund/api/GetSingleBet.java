package com.wolfshund.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;

import com.wolfshund.api.model.BetData;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.utils.Const;

public class GetSingleBet extends Client{
	
	private static String API_URL = Const.ApiUrl.SINGLE_BET;

	public GetSingleBet(Activity activity, ApiListener listener){
		super(activity);
		setApiListener(listener);
	}
	
	public void runClient(int betId){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(Const.SingleBetModel.BET_ID, String.valueOf(betId)));
		
		runPostClient(API_URL, BetData.class, params);
	}
}
