package com.wolfshund.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;

import com.wolfshund.api.model.UserData;
import com.wolfshund.api.model.UserDataExt;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.utils.Const;

public class GetUserData extends Client{
	
	private static String API_URL = Const.ApiUrl.USER_DATA;
	private static String API_URL_EXT = Const.ApiUrl.GET_USER_DATA_EXT;

	public GetUserData(Activity activity, ApiListener listener){
		super(activity);
		setApiListener(listener);
	}
	
	public void runClient(int userId){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(Const.UserDataModel.USER_ID, String.valueOf(userId)));
		
		runPostClient(API_URL, UserData.class, params);
	}
	
	public void runClientExt(String uuid){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(Const.UserDataModel.UUID, String.valueOf(uuid)));
		
		runPostClient(API_URL_EXT, UserDataExt.class, params);
	}
}
