package com.wolfshund.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;

import com.wolfshund.api.model.MasterData;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.utils.Const;

public class GetMasterData extends Client{
	
	private static String API_URL = Const.ApiUrl.MASTER_DATA;

	public GetMasterData(Activity activity, ApiListener listener){
		super(activity);
		setApiListener(listener);
	}
	
	public void runClient(String uuid){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(Const.MasterDataModel.UUID, uuid));
		
		runPostClient(API_URL, MasterData.class, params);
	}
}
