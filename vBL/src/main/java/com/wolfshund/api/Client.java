package com.wolfshund.api;

import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.wolfshund.api.model.BaseModel;
import com.wolfshund.dialog.AppDialog;
import com.wolfshund.listeners.ApiListener;
import com.wolfshund.listeners.DialogListener;
import com.wolfshund.listeners.ErrorApiListener;
import com.wolfshund.utils.Const;
import com.wolfshund.vbl.R;

public class Client {
	
	private static final int HTTPGET = 1;
	private static final int HTTPPOST = 2;
	
	protected Class<BaseModel> mResultObjectClass;
	protected int mHttpType = HTTPGET;
	protected BaseClient mBaseClient;
	protected String mUrl;
	protected List<NameValuePair> mParams;
	protected static final Gson sGson = new Gson();
	private ApiListener apiListener;
	protected boolean mShowErrorDialog = true;
	private Activity activity;
	private ErrorApiListener mErrorListener;
	
	protected Client(Activity activity) {
		if (activity == null)
			throw new NullPointerException("Activity and/or its Context"
					+ " must not be null when Client is initializing");

		this.activity = activity;

	}
	
	protected void runGetClient(String url, Class resultObjectClass) {
		mResultObjectClass = resultObjectClass;
		mHttpType = HTTPGET;
		mUrl = url;
		runClient();
	}
	
	protected void runPostClient(String url, Class resultObjectClass, List<NameValuePair> params) {
		mResultObjectClass = resultObjectClass;
		mHttpType = HTTPPOST;
		mUrl = url;
		mParams = params;
		runClient();
	}
	
	private void runClient() {
		new ClientAsync().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
	}
	
	public BaseClient getHttpGet() {
		if (mBaseClient == null || !(mBaseClient instanceof GetClient)) {
			mBaseClient = new GetClient();
		}
		return mBaseClient;
	}

	public BaseClient getHttpPost() {
		if (mBaseClient == null || !(mBaseClient instanceof PostClient)) {
			mBaseClient = new PostClient();
		}
		return mBaseClient;
	}
	
	public ApiListener getApiListener() {
		return apiListener;
	}
	
	public void setApiListener(ApiListener listener) {
		apiListener = listener;
	}
	
	public ErrorApiListener getErrorListener() {
		return mErrorListener;
	}
	
	public void setErrorListener(ErrorApiListener listener) {
		mErrorListener = listener;
	}
	
	protected void showErrorDialog(String errorMessage) {
		if (errorMessage == null || errorMessage.length() == 0) {
			errorMessage = Const.Error.DEFAULT_MESSAGE;
		}
		
		AppDialog dialog = new AppDialog(activity, errorMessage, activity.getString(R.string.close), activity.getString(R.string.retry));
		dialog.setListener(new DialogListener() {
			
			@Override
			public void onOkClicked(Dialog dialog) {
				if(mErrorListener != null) mErrorListener.onCancel();
				dialog.dismiss();
			}
			
			@Override
			public void onCancelClicked(Dialog dialog) {
				if(mErrorListener != null) mErrorListener.onRetry();
				dialog.dismiss();
				runClient();
			}
		});
		
		dialog.show();
	}
	
	protected class ClientAsync extends AsyncTask<Void, Void, Boolean>{
		
		protected JSONObject mJsonResponse;
		protected Object mObjectResult;
		protected String mErrorMessage;

		@Override
		protected Boolean doInBackground(Void... params) {
			if(mHttpType == HTTPGET){
				mJsonResponse = getHttpGet().getJsonObject(mUrl);
			}else if(mHttpType == HTTPPOST){
				mJsonResponse = getHttpPost().getJsonObject(mUrl, mParams);
			}
			
			if (isClientError(mJsonResponse)) {
				getErrorMessage(mJsonResponse);
				return false;
			} else
				return getObject();
		}
		
		protected boolean getObject() {
			mObjectResult = sGson.fromJson(mJsonResponse.toString(), mResultObjectClass);
			return mObjectResult != null;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (getApiListener() != null) {
				if (result) {
					getApiListener().onSuccess(mObjectResult);
				} else {
					getApiListener().onError(mErrorMessage);
					if (mShowErrorDialog) {
						showErrorDialog(mErrorMessage);
					}
				}
			}
		}
		
		private void getErrorMessage(JSONObject jsonResult) {
			if (jsonResult.has(Const.BaseModel.MESSAGE)) {
				try {
					String message = jsonResult.getString(Const.BaseModel.MESSAGE);
					if (message != null && !"".equals(message)) {
						mErrorMessage = message;
					}else{
						mErrorMessage = Const.Error.DEFAULT_MESSAGE;
					}
				} catch (JSONException e) {
					e.printStackTrace();
					mErrorMessage = Const.Error.DEFAULT_MESSAGE;
				}

			}
		}
	}
	
	protected boolean isClientError(JSONObject jsonResult) {
		if (jsonResult == null) {
			return true;
		}
		
		try {
			if(jsonResult.getInt(Const.BaseModel.RESULT) < 1){
				return true;
			}
			
			if (!jsonResult.has(Const.BaseModel.DATA)) {
				return true;
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
			return true;
		}
		
		return false;
	}
	
}
