package com.wolfshund.api.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class MasterInformation implements Serializable{
	private static final long serialVersionUID = -3309016379467268064L;
	
	@SerializedName(Const.MasterDataModel.CURRENT_ROUND)
	private List<RoundInformation> currentRound;
	
	@SerializedName(Const.MasterDataModel.NEXT_ROUND)
	private List<RoundInformation> nextRound;
	
	@SerializedName(Const.MasterDataModel.PREV_ROUND)
	private List<RoundInformation> prevRound;
	
	@SerializedName(Const.MasterDataModel.TABLE)
	private List<LeagueTableInformation> table;
	
	@SerializedName(Const.MasterDataModel.USER)
	private UserInformation user;
	
	@SerializedName(Const.MasterDataModel.BETS)
	private List<UserBetsInformation> bets;
	
	@SerializedName(Const.MasterDataModel.USERS_P_POINTS)
	private List<UserInformation> usersPPoints;
	
	@SerializedName(Const.MasterDataModel.USERS_POINTS)
	private List<UserInformation> usersPoints;
	
	@SerializedName(Const.MasterDataModel.USERS_POINTS_TEN_GAMES)
	private List<UserInformation> usersPointsTenGames;

	public List<RoundInformation> getCurrentRound() {
		return currentRound;
	}

	public void setCurrentRound(List<RoundInformation> currentRound) {
		this.currentRound = currentRound;
	}

	public List<RoundInformation> getNextRound() {
		return nextRound;
	}

	public void setNextRound(List<RoundInformation> nextRound) {
		this.nextRound = nextRound;
	}
	
	public List<RoundInformation> getPrevRound() {
		return prevRound;
	}

	public void setPrevRound(List<RoundInformation> prevRound) {
		this.prevRound = prevRound;
	}

	public List<LeagueTableInformation> getTable() {
		return table;
	}

	public void setTable(List<LeagueTableInformation> table) {
		this.table = table;
	}

	public UserInformation getUser() {
		return user;
	}

	public void setUser(UserInformation user) {
		this.user = user;
	}

	public List<UserBetsInformation> getBets() {
		return bets;
	}

	public void setBets(List<UserBetsInformation> bets) {
		this.bets = bets;
	}

	public List<UserInformation> getUsersPPoints() {
		return usersPPoints;
	}

	public void setUsersPPoints(List<UserInformation> usersPPoints) {
		this.usersPPoints = usersPPoints;
	}

	public List<UserInformation> getUsersPoints() {
		return usersPoints;
	}

	public void setUsersPoints(List<UserInformation> usersPoints) {
		this.usersPoints = usersPoints;
	}

	public List<UserInformation> getUsersPointsTenGames() {
		return usersPointsTenGames;
	}

	public void setUsersPointsTenGames(List<UserInformation> usersPointsTenGames) {
		this.usersPointsTenGames = usersPointsTenGames;
	}

}
