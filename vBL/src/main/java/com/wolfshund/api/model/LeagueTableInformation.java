package com.wolfshund.api.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class LeagueTableInformation implements Serializable{
	private static final long serialVersionUID = -3309016379467268064L;
	
	@SerializedName(Const.LeagueTableModel.ID)
	private int id;
	
	@SerializedName(Const.LeagueTableModel.CLUB_ID)
	private int clubId;
	
	@SerializedName(Const.LeagueTableModel.POINTS)
	private int points;
	
	@SerializedName(Const.LeagueTableModel.WIN_GAMES)
	private int winGames;
	
	@SerializedName(Const.LeagueTableModel.DRAW_GAMES)
	private int drawGames;
	
	@SerializedName(Const.LeagueTableModel.LOSE_GAMES)
	private int loseGames;
	
	@SerializedName(Const.LeagueTableModel.GAMES)
	private int games;
	
	@SerializedName(Const.LeagueTableModel.GOALS_ALLOWED)
	private int goalsAllowed;
	
	@SerializedName(Const.LeagueTableModel.GOALS_SCORED)
	private int goalsScored;
	
	@SerializedName(Const.LeagueTableModel.ID_SCHEDULE)
	private String idSchedule;
	
	@SerializedName(Const.LeagueTableModel.NAME)
	private String name;
	
	@SerializedName(Const.LeagueTableModel.SHORT_NAME)
	private String shortName;
	
	@SerializedName(Const.LeagueTableModel.OFFENCE)
	private int offence;
	
	@SerializedName(Const.LeagueTableModel.DEFENCE)
	private int defence;
	
	@SerializedName(Const.LeagueTableModel.INDEX_QUALITY)
	private int indexQuality;
	
	@SerializedName(Const.LeagueTableModel.FORM)
	private double form;
	
	@SerializedName(Const.LeagueTableModel.LAST_FIVE_FORM)
	private String lastFiveForm;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClubId() {
		return clubId;
	}

	public void setClubId(int clubId) {
		this.clubId = clubId;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public int getWinGames() {
		return winGames;
	}

	public void setWinGames(int winGames) {
		this.winGames = winGames;
	}

	public int getDrawGames() {
		return drawGames;
	}

	public void setDrawGames(int drawGames) {
		this.drawGames = drawGames;
	}

	public int getLoseGames() {
		return loseGames;
	}

	public void setLoseGames(int loseGames) {
		this.loseGames = loseGames;
	}

	public int getGames() {
		return games;
	}

	public void setGames(int games) {
		this.games = games;
	}

	public int getGoalsAllowed() {
		return goalsAllowed;
	}

	public void setGoalsAllowed(int goalsAllowed) {
		this.goalsAllowed = goalsAllowed;
	}

	public int getGoalsScored() {
		return goalsScored;
	}

	public void setGoalsScored(int goalsScored) {
		this.goalsScored = goalsScored;
	}

	public String getIdSchedule() {
		return idSchedule;
	}

	public void setIdSchedule(String idSchedule) {
		this.idSchedule = idSchedule;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public int getOffence() {
		return offence;
	}

	public void setOffence(int offence) {
		this.offence = offence;
	}

	public int getDefence() {
		return defence;
	}

	public void setDefence(int defence) {
		this.defence = defence;
	}

	public int getIndexQuality() {
		return indexQuality;
	}

	public void setIndexQuality(int indexQuality) {
		this.indexQuality = indexQuality;
	}

	public double getForm() {
		return form;
	}

	public void setForm(double form) {
		this.form = form;
	}

	public String getLastFiveForm() {
		return lastFiveForm;
	}

	public void setLastFiveForm(String lastFiveForm) {
		this.lastFiveForm = lastFiveForm;
	}
	
}
