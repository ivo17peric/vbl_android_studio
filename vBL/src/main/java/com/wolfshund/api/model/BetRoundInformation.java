package com.wolfshund.api.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class BetRoundInformation implements Serializable{
	private static final long serialVersionUID = -3309016379467268064L;
	
	@SerializedName(Const.BetRoundModel.SEASON)
	private int season;
	
	@SerializedName(Const.BetRoundModel.ROUND)
	private int round;
	
	@SerializedName(Const.BetRoundModel.HOME_NAME)
	private String homeName;
	
	@SerializedName(Const.RoundModel.HOME_OFFENCE)
	private int homeOffence;
	
	@SerializedName(Const.RoundModel.HOME_DEFENCE)
	private int homeDefence;
	
	@SerializedName(Const.RoundModel.HOME_LAST_FIVE_FORM)
	private String homeLastFiveForm;
	
	@SerializedName(Const.RoundModel.HOME_FORM)
	private double homeForm;
	
	@SerializedName(Const.BetRoundModel.AWAY_NAME)
	private String awayName;
	
	@SerializedName(Const.RoundModel.AWAY_OFFENCE)
	private int awayOffence;
	
	@SerializedName(Const.RoundModel.AWAY_DEFENCE)
	private int awayDefence;
	
	@SerializedName(Const.RoundModel.AWAY_LAST_FIVE_FORM)
	private String awayLastFiveForm;
	
	@SerializedName(Const.RoundModel.AWAY_FORM)
	private double awayForm;
	
	@SerializedName(Const.BetRoundModel.BETS)
	private BetType bets;
	
	@SerializedName(Const.BetRoundModel.MATCH_ID)
	private int matchId;
	
	@SerializedName(Const.RoundModel.DATE_MATCH)
	private String dateMatch;

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public String getHomeName() {
		return homeName;
	}

	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}

	public String getAwayName() {
		return awayName;
	}

	public void setAwayName(String awayName) {
		this.awayName = awayName;
	}

	public BetType getBets() {
		return bets;
	}

	public void setBets(BetType bets) {
		this.bets = bets;
	}

	public int getMatchId() {
		return matchId;
	}

	public void setMatchId(int matchId) {
		this.matchId = matchId;
	}

	public int getHomeOffence() {
		return homeOffence;
	}

	public void setHomeOffence(int homeOffence) {
		this.homeOffence = homeOffence;
	}

	public int getHomeDefence() {
		return homeDefence;
	}

	public void setHomeDefence(int homeDefence) {
		this.homeDefence = homeDefence;
	}

	public String getHomeLastFiveForm() {
		return homeLastFiveForm;
	}

	public void setHomeLastFiveForm(String homeLastFiveForm) {
		this.homeLastFiveForm = homeLastFiveForm;
	}

	public int getAwayOffence() {
		return awayOffence;
	}

	public void setAwayOffence(int awayOffence) {
		this.awayOffence = awayOffence;
	}

	public int getAwayDefence() {
		return awayDefence;
	}

	public void setAwayDefence(int awayDefence) {
		this.awayDefence = awayDefence;
	}

	public String getAwayLastFiveForm() {
		return awayLastFiveForm;
	}

	public void setAwayLastFiveForm(String awayLastFiveForm) {
		this.awayLastFiveForm = awayLastFiveForm;
	}

	public String getDateMatch() {
		return dateMatch;
	}

	public void setDateMatch(String dateMatch) {
		this.dateMatch = dateMatch;
	}

	public double getHomeForm() {
		return homeForm;
	}

	public void setHomeForm(double homeForm) {
		this.homeForm = homeForm;
	}

	public double getAwayForm() {
		return awayForm;
	}

	public void setAwayForm(double awayForm) {
		this.awayForm = awayForm;
	}
	
}
