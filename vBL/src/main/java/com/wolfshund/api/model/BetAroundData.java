package com.wolfshund.api.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class BetAroundData extends BaseModel implements Serializable{
	
	private static final long serialVersionUID = 1835413460501315034L;
	
	@SerializedName(Const.BetRoundModel.BETS)
	private List<BetInformation> betInformationList;
	
	@SerializedName(Const.MasterDataModel.USER)
	private UserInformation userData;
	
	@SerializedName(Const.BetRoundModel.BET_NUM)
	private int numOfBet;

	public List<BetInformation> getBetInformationList() {
		return betInformationList;
	}

	public void setBetInformationList(List<BetInformation> betInformationList) {
		this.betInformationList = betInformationList;
	}

	public UserInformation getUserData() {
		return userData;
	}

	public void setUserData(UserInformation userData) {
		this.userData = userData;
	}

	public int getNumOfBet() {
		return numOfBet;
	}

	public void setNumOfBet(int numOfBet) {
		this.numOfBet = numOfBet;
	}

}
