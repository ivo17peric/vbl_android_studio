package com.wolfshund.api.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class LeagueTableData extends BaseModel implements Serializable{
	
	private static final long serialVersionUID = 1835413460501315034L;
	
	@SerializedName(Const.BaseModel.DATA)
	private List<LeagueTableInformation> leagueDataList;
	
	public List<LeagueTableInformation> getLeagueTableData() {
		return leagueDataList;
	}

	public void setLeagueTalbeData(List<LeagueTableInformation> leagueDataList) {
		this.leagueDataList = leagueDataList;
	}

}
