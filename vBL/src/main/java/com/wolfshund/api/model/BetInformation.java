package com.wolfshund.api.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class BetInformation implements Serializable{
	private static final long serialVersionUID = -3309016379467268064L;
	
	@SerializedName(Const.SingleBetModel.HOME_NAME)
	private String homeName;
	
	@SerializedName(Const.SingleBetModel.AWAY_NAME)
	private String awayName;
	
	@SerializedName(Const.SingleBetModel.BET)
	private int bet;
	
	@SerializedName(Const.SingleBetModel.RESULT)
	private int result;
	
	@SerializedName(Const.SingleBetModel.HOME_GOALS)
	private int homeGoals;
	
	@SerializedName(Const.SingleBetModel.AWAY_GOALS)
	private int awayGoals;
	
	@SerializedName(Const.SingleBetModel.BET_PLACE)
	private int betPlace;
	
	@SerializedName(Const.SingleBetModel.KOEF)
	private double koef;
	
	@SerializedName(Const.SingleBetModel.PRICE)
	private double price;
	
	@SerializedName(Const.SingleBetModel.HALF_PRICE)
	private double halfPrice;

	public String getHomeName() {
		return homeName;
	}

	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}

	public String getAwayName() {
		return awayName;
	}

	public void setAwayName(String awayName) {
		this.awayName = awayName;
	}

	public int getBet() {
		return bet;
	}

	public void setBet(int bet) {
		this.bet = bet;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getHomeGoals() {
		return homeGoals;
	}

	public void setHomeGoals(int homeGoals) {
		this.homeGoals = homeGoals;
	}

	public int getAwayGoals() {
		return awayGoals;
	}

	public void setAwayGoals(int awayGoals) {
		this.awayGoals = awayGoals;
	}

	public int getBetPlace() {
		return betPlace;
	}

	public void setBetPlace(int betPlace) {
		this.betPlace = betPlace;
	}

	public double getKoef() {
		return koef;
	}

	public void setKoef(double koef) {
		this.koef = koef;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getHalfPrice() {
		return halfPrice;
	}

	public void setHalfPrice(double halfPrice) {
		this.halfPrice = halfPrice;
	}
	
}
