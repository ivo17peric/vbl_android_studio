package com.wolfshund.api.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class BetRoundData extends BaseModel implements Serializable{
	
	private static final long serialVersionUID = 1835413460501315034L;
	
	@SerializedName(Const.BaseModel.DATA)
	private BetRoundAroundData dataM;

	public BetRoundAroundData getDataM() {
		return dataM;
	}

	public void setDataM(BetRoundAroundData dataM) {
		this.dataM = dataM;
	}

}
