package com.wolfshund.api.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class RoundInformation implements Serializable{
	private static final long serialVersionUID = -3309016379467268064L;
	
	@SerializedName(Const.RoundModel.HOME)
	private String home;
	
	@SerializedName(Const.RoundModel.HOME_OFFENCE)
	private int homeOffence;
	
	@SerializedName(Const.RoundModel.HOME_DEFENCE)
	private int homeDefence;
	
	@SerializedName(Const.RoundModel.HOME_LAST_FIVE_FORM)
	private String homeLastFiveForm;
	
	@SerializedName(Const.RoundModel.AWAY)
	private String away;
	
	@SerializedName(Const.RoundModel.AWAY_OFFENCE)
	private int awayOffence;
	
	@SerializedName(Const.RoundModel.AWAY_DEFENCE)
	private int awayDefence;
	
	@SerializedName(Const.RoundModel.AWAY_LAST_FIVE_FORM)
	private String awayLastFiveForm;
	
	@SerializedName(Const.RoundModel.ID)
	private int id;
	
	@SerializedName(Const.RoundModel.ROUND)
	private int round;
	
	@SerializedName(Const.RoundModel.SEASON)
	private int season;
	
	@SerializedName(Const.RoundModel.HOME_GOALS)
	private int homeGoals;
	
	@SerializedName(Const.RoundModel.AWAY_GOALS)
	private int awayGoals;
	
	@SerializedName(Const.RoundModel.DATE_MATCH)
	private String dateMatch;

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getAway() {
		return away;
	}

	public void setAway(String away) {
		this.away = away;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public int getHomeGoals() {
		return homeGoals;
	}

	public void setHomeGoals(int homeGoals) {
		this.homeGoals = homeGoals;
	}

	public int getAwayGoals() {
		return awayGoals;
	}

	public void setAwayGoals(int awayGoals) {
		this.awayGoals = awayGoals;
	}

	public String getDateMatch() {
		return dateMatch;
	}

	public void setDateMatch(String dateMatch) {
		this.dateMatch = dateMatch;
	}

	public int getHomeOffence() {
		return homeOffence;
	}

	public void setHomeOffence(int homeOffence) {
		this.homeOffence = homeOffence;
	}

	public int getHomeDefence() {
		return homeDefence;
	}

	public void setHomeDefence(int homeDefence) {
		this.homeDefence = homeDefence;
	}

	public String getHomeLastFiveForm() {
		return homeLastFiveForm;
	}

	public void setHomeLastFiveForm(String homeLastFiveForm) {
		this.homeLastFiveForm = homeLastFiveForm;
	}

	public int getAwayOffence() {
		return awayOffence;
	}

	public void setAwayOffence(int awayOffence) {
		this.awayOffence = awayOffence;
	}

	public int getAwayDefence() {
		return awayDefence;
	}

	public void setAwayDefence(int awayDefence) {
		this.awayDefence = awayDefence;
	}

	public String getAwayLastFiveForm() {
		return awayLastFiveForm;
	}

	public void setAwayLastFiveForm(String awayLastFiveForm) {
		this.awayLastFiveForm = awayLastFiveForm;
	}
	
}
