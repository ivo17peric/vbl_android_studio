package com.wolfshund.api.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.wolfshund.utils.Const;

public class BetRoundAroundData extends BaseModel implements Serializable{
	
	private static final long serialVersionUID = 1835413460501315034L;
	
	@SerializedName(Const.BetRoundModel.BETS)
	private List<BetRoundInformation> betRoundInformationList;
	
	@SerializedName(Const.UserDataModel.P_POINTS)
	private long pPoints;

	public List<BetRoundInformation> getBetRoundInformationList() {
		return betRoundInformationList;
	}

	public void setBetRoundInformationList(List<BetRoundInformation> betRoundInformationList) {
		this.betRoundInformationList = betRoundInformationList;
	}

	public long getpPoints() {
		return pPoints;
	}

	public void setpPoints(long pPoints) {
		this.pPoints = pPoints;
	}
	
}
