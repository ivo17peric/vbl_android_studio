package com.wolfshund.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wolfshund.api.model.UserInformation;
import com.wolfshund.utils.Const;
import com.wolfshund.utils.Utils;
import com.wolfshund.vbl.BaseActivity;
import com.wolfshund.vbl.R;
import com.wolfshund.vbl.UsersFragment;
import com.wolfshund.view.SimpleAutoFitTextView;

public class UsersAdvanceAdapter extends BaseAdapter {
	
	private List<UserInformation> mDataList;
	private LayoutInflater mInflater;
	private Context mContext;
	private int mType;
	private int listType;
	
	public UsersAdvanceAdapter (Context context){
		mContext = context;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void setData(List<UserInformation> list){
		mDataList = list;
	}
	
	public void addData(List<UserInformation> list){
		mDataList.addAll(list);
		this.notifyDataSetChanged();
	}
	
	public void addDataToTop(List<UserInformation> list){
		mDataList.addAll(0, list);
		this.notifyDataSetChanged();
	}
	
	public void setType(int type, int listType){
		mType = type;
		this.listType = listType;
	}

	@Override
	public int getCount() {
		if(mDataList != null) {
			if(listType == Const.ListType.TOP){
				return mDataList.size() + 1;
			}else{
				return mDataList.size() + 2;
			}
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if(mDataList != null) {
			if(listType == Const.ListType.TOP){
				if(position > mDataList.size() - 1){
					return null;
				}
				return mDataList.get(position);
			}else{
				if(position > mDataList.size() || position == 0){
					return null;
				}
				return mDataList.get(position - 1);
			}
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		if(mDataList != null) {
			if(listType == Const.ListType.TOP){
				if(position > mDataList.size() - 1){
					return 0;
				}
				return mDataList.get(position).getId();
			}else{
				if(position > mDataList.size() || position == 0){
					return 0;
				}
				return mDataList.get(position - 1).getId();
			}
		}
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder;
		
		if(convertView == null){
			holder = new ViewHolder();
			
			convertView = mInflater.inflate(R.layout.item_top_users, null, false);
			Utils.scaleToFit((Activity) mContext, convertView);
			
			holder.tvPosition = (TextView) convertView.findViewById(R.id.position);
			holder.tvName = (TextView) convertView.findViewById(R.id.userName);
			holder.tvPoints = (TextView) convertView.findViewById(R.id.mainPoints);
			holder.tvLeftSubLabel = (TextView) convertView.findViewById(R.id.leftSubPointsLabel);
			holder.tvLeftSubPoints = (TextView) convertView.findViewById(R.id.leftSubPoints);
			holder.tvRightSubLabel = (TextView) convertView.findViewById(R.id.rightSubPointsLabel);
			holder.tvRightSubPoints = (TextView) convertView.findViewById(R.id.rightSubPoints);
			holder.tvLoadNext = (TextView) convertView.findViewById(R.id.loadNext);
			holder.content = convertView.findViewById(R.id.contentRL);
			
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		if(position % 2 == 0){
			convertView.setBackgroundColor(mContext.getResources().getColor(R.color.even_row));
		}else{
			convertView.setBackgroundColor(mContext.getResources().getColor(R.color.odd_row));
		}
		
		if(position == 0 && listType == Const.ListType.AROUND){
			holder.content.setVisibility(View.INVISIBLE);
			holder.tvLoadNext.setVisibility(View.VISIBLE);
			holder.tvLoadNext.setText("Load Previous");
			holder.tvLoadNext.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
			return convertView;
		}else if (listType == Const.ListType.AROUND && position > mDataList.size()){
			holder.content.setVisibility(View.INVISIBLE);
			holder.tvLoadNext.setVisibility(View.VISIBLE);
			holder.tvLoadNext.setText("Load Next");
			holder.tvLoadNext.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
			return convertView;
		}else if(listType == Const.ListType.TOP && position > mDataList.size() - 1){
			holder.content.setVisibility(View.INVISIBLE);
			holder.tvLoadNext.setVisibility(View.VISIBLE);
			holder.tvLoadNext.setText("Load Next");
			holder.tvLoadNext.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
			return convertView;
		}
		
		holder.content.setVisibility(View.VISIBLE);
		holder.tvLoadNext.setVisibility(View.INVISIBLE);
		
		UserInformation user;
		if(listType == Const.ListType.AROUND){
			user = mDataList.get(position - 1);
			holder.tvPosition.setText(user.getPosMy() + ".");
		}else{
			user = mDataList.get(position);
			holder.tvPosition.setText((position + 1) + ".");
		}
		
		holder.tvName.setText(user.getUsername());
		
		switch (mType) {
		case UsersFragment.SELECTED_LAST_TEN:
			holder.tvPoints.setText(String.valueOf(user.getPointsTenGames()));
			holder.tvLeftSubLabel.setText("Users points:");
			holder.tvLeftSubPoints.setText(String.valueOf(user.getpPoints()));
			holder.tvRightSubLabel.setText("All time points:");
			holder.tvRightSubPoints.setText(String.valueOf(user.getPoints()));
			break;
		case UsersFragment.SELECTED_P_POINTS:
			holder.tvPoints.setText(String.valueOf(user.getpPoints()));
			holder.tvLeftSubLabel.setText("Ten games points:");
			holder.tvLeftSubPoints.setText(String.valueOf(user.getPointsTenGames()));
			holder.tvRightSubLabel.setText("All time points:");
			holder.tvRightSubPoints.setText(String.valueOf(user.getPoints()));
			break;
		case UsersFragment.SELECTED_POINTS:
			holder.tvPoints.setText(String.valueOf(user.getPoints()));
			holder.tvLeftSubLabel.setText("Ten games points:");
			holder.tvLeftSubPoints.setText(String.valueOf(user.getPointsTenGames()));
			holder.tvRightSubLabel.setText("Users points:");
			holder.tvRightSubPoints.setText(String.valueOf(user.getpPoints()));
			break;

		default:
			break;
		}
		
		if(user.getUuid().equals(((BaseActivity)mContext).getPreferences().getUuid())){
			holder.tvName.setTextColor(mContext.getResources().getColor(R.color.win_green));
			holder.tvPoints.setTextColor(mContext.getResources().getColor(R.color.win_green));
			holder.tvPosition.setTextColor(mContext.getResources().getColor(R.color.win_green));
		}else{
			holder.tvName.setTextColor(Color.WHITE);
			holder.tvPoints.setTextColor(Color.WHITE);
			holder.tvPosition.setTextColor(Color.WHITE);
		}
		
		holder.tvPosition.setTextSize(TypedValue.COMPLEX_UNIT_PX, 40 * Utils.getK());
		((SimpleAutoFitTextView)holder.tvPosition).callOnMeasure();
		holder.tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX, 40 * Utils.getK());
		((SimpleAutoFitTextView)holder.tvName).callOnMeasure();
		holder.tvPoints.setTextSize(TypedValue.COMPLEX_UNIT_PX, 40 * Utils.getK());
		((SimpleAutoFitTextView)holder.tvPoints).callOnMeasure();
		holder.tvLeftSubLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, 20 * Utils.getK());
		((SimpleAutoFitTextView)holder.tvLeftSubLabel).callOnMeasure();
		holder.tvLeftSubPoints.setTextSize(TypedValue.COMPLEX_UNIT_PX, 20 * Utils.getK());
		((SimpleAutoFitTextView)holder.tvLeftSubPoints).callOnMeasure();
		holder.tvRightSubLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, 20 * Utils.getK());
		((SimpleAutoFitTextView)holder.tvRightSubLabel).callOnMeasure();
		holder.tvRightSubPoints.setTextSize(TypedValue.COMPLEX_UNIT_PX, 20 * Utils.getK());
		((SimpleAutoFitTextView)holder.tvRightSubPoints).callOnMeasure();
		
		return convertView;
	}
	
	class ViewHolder {
		public TextView tvPosition;
		public TextView tvName;
		public TextView tvPoints;
		public TextView tvLeftSubLabel;
		public TextView tvLeftSubPoints;
		public TextView tvRightSubLabel;
		public TextView tvRightSubPoints;
		public TextView tvLoadNext;
		
		public View content;
	}

}
