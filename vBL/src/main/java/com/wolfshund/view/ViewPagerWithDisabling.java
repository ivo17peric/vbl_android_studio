package com.wolfshund.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ViewPagerWithDisabling extends ViewPager {
	
	private boolean mIsEnabled = true;

	public ViewPagerWithDisabling(Context context) {
		super(context);
	}
	
	public ViewPagerWithDisabling(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.mIsEnabled) {
            return super.onTouchEvent(event);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.mIsEnabled) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    public void setPagingEnabled(boolean b) {
        this.mIsEnabled = b;
    }

}
